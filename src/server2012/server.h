/**
 *  @file server.h
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *  
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Hlavičkový soubor serveru, popisuje třídy pServer a pThread
 * 
 */
 
#ifndef _SERVER_H
#define _SERVER_H

#include <QThread>
#include <QTcpSocket>
#include <QTcpServer>

#include <QList>
#include <QByteArray>

#include "../pn2012/place.h"
#include "../pn2012/trans.h"
#include "../pn2012/link.h"

/*!
 * @brief Datová struktura ulehčující uchovávat dočasnou informaci o linku
 */
typedef struct sLink {
    int place;  /*!< Místo, s nímž je přechod spojen */
    QString lambda; /*!< Lambda výraz na spojení */
} sLink;

/*!
 * @brief Třída implementující tcp server, který je schopný simulovat Petriho síť
 */
class pServer : public QTcpServer
{
    Q_OBJECT
    
    public:
        explicit pServer(QObject *parent = 0);
        void startServer(int port);
        ~pServer();
        
    public slots:
    
    private:
        QTcpServer server;
        
    protected:
        void incomingConnection(int socketDescriptor);
};

/*!
 * @brief Třída vlákna na serveru
 * Třída implementující vlákno, které na serveru přijme data, provede zadaný výpočet
 * (krokově nebo celý), a odešle přepočítaný data zpět klientovi
 */
class pThread : public QThread
{
    Q_OBJECT
    
    public:
        pThread(int socD, QObject *parent = 0);
        void xmlload(QByteArray);
        QByteArray xmlgen();
        
    public slots:
        void end_sock();
        void calculate();
    
    signals:
    
    protected:
        void run();
    
    private:
        void fullSim();
        
        QTcpSocket *socket;
        int socDesc;
        
        int marked;
        bool authorized;
        
        QList<Trans *> myTransList;
        QList<Place *> myPlacesList;
    
};

#endif
