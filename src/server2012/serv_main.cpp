/**
 *  @file serv_main.cpp
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *  
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Zdrojový kód ke spuštění serveru a načtení jeho argumentů
 * 
 */

#include <QtCore>
#include <QString>

#include <iostream>

#include "server.h"

const char *hlp = "\
Server spouštějte takto:\n\
    ./server port\n\
        port - číslo portu, na kterém má server běžet\n\
";

int main(int argc, char **argv) {
    // aby byl server schopný reagovat na události, je třeba ho spustit v 
    // QCoreApplication
    QCoreApplication app(argc, argv);
    if(argc != 2) {
        std::cout << hlp;
        std::cerr << "Zadány špatné argumenty, vypínám aplikaci.\n\n";
        return EXIT_FAILURE;
    }
    QString convert = argv[1];
    pServer server;
    // Zapneme server
    server.startServer(convert.toInt());
    return app.exec();
}
