/**
 *  @file server.cpp
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *  
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Modul popisující chování serveru a threadů, které provádějí
 *  samotný výpočet
 * 
 * @todo Načtení loginů ze souboru, správa verze sítí jednotlivých uživatelů,
 *       jejich popis, není logování na serveru
 * 
 */
 
#include <QTcpSocket>
#include <QThread>
#include <QDebug>

#include <QXmlStreamReader>
#include <QString>

#include <iostream>
#include <vector>

#include "server.h"

using namespace std;

QString login("admin");
QString passw("milous");

pServer::pServer(QObject *parent) :
    QTcpServer(parent)
{

}

void pServer::startServer(int port)
{
    /**
     * Provede start serveru, nepodaří-li se odposlouchávat na daném portu,
     * aplikace se ukončí.
     */
    if(!this->listen(QHostAddress::Any,port)) {
        std::cerr << "Error starting server.\n";
        exit(0);
    }
}

void pServer::incomingConnection(int socketDescriptor)
{
    /**
     * Implementuje chování při příchozím spojení na server - spustí
     * nový thread schopný simulovat Petriho síť
     */
    pThread *newThread = new pThread(socketDescriptor, this);
    connect(newThread, SIGNAL(finished()), newThread, SLOT(deleteLater()));
    newThread->start();
    //newThread.wait();
}


pServer::~pServer() {
    server.close();
}

pThread::pThread(int socD, QObject *parent) : QThread(parent) 
{
    /**
     * @param socD Popisovač již existujícího socketu
     * 
     * Vytvoří thread, přiřadí do jeho soukromé proměnné informaci o 
     * popisovači socketu.
     */
    socDesc = socD;
    authorized = false;     // klient ještě nebyl rozeznán
}

void pThread::run() {
    /**
     * Popisuje chování threadu - přidělí popisovač threadovému socketu,
     * čeká, dokud se na něm neobjeví nějaký vstup.
     */
    socket = new QTcpSocket;
    connect(socket, SIGNAL(disconnected()), this, SLOT(end_sock()), Qt::DirectConnection);
    socket->setSocketDescriptor(socDesc);   // propojení socketu se získaným popisovačem
    connect(socket, SIGNAL(readyRead()), this, SLOT(calculate()), Qt::DirectConnection);
    
    exec();
}

void pThread::end_sock() {
    /**
     * Slot pro uzavření komunikace
     */
    socket->deleteLater();
    exit(0);
}

void pThread::calculate() {
    /**
     * Slot pro samotnou simulaci Petriho sítí ze vstupu, který je očekáván
     * na socketu.
     * Po provedení simulace zašle přepočítaná data zpět klientovi.
     * 
     * @todo Zavést soubor s hesly, vzdálený přístup k adresáři serveru včetně
     *       otevírání a uzavírání souborů, navázání proměnných při krokové
     *       simulaci
     */
    QByteArray data, dataAuth;
    
    // Nyní očekáváme xml data ze serveru
    data = socket->readAll();
    socket->flush();
    
    // Pokud ještě klient není autorizován pro přístup k výpočtu
    if(!authorized) {
        // Vytvoř a pošli žádost o autentifikaci
        QString auth("whoareu");
        socket->flush();
        socket->write((const char *)auth.toLatin1(), auth.size());
        if(!socket->waitForReadyRead())
            end_sock();
        // Zatím je zde pouze jedno heslo napevno, tím pádem lze kontrolovat takto
        dataAuth = socket->readAll();
        if(dataAuth != QByteArray("admin:milous")) {
            // Pokud je heslo zadáno špatně, neprovedeme výpočet
            socket->write(data);
            return;
        }
        else authorized = true;
    }
    xmlload(data);
    // Rozhodování, zdali se má krokovat
    if(marked > -1) {
        myTransList[marked]->simulate();
    }
    else fullSim();
    
    // Zapiš výsledek a odešli
    QString result = xmlgen();
    socket->flush();
    socket->write((const char *)result.toLatin1(), result.size());
}

void pThread::fullSim() {
    /**
     * Provede celou simulaci až do finálního stavu.
     * 
     * @todo Zatím zvládá pouze max 200 kroků
     */
    int running = 1;
    int step = 0;
    int maxStep = 200;
    while(running && (step < maxStep)) {
        running = 0;
        // pro kazdy prechod zavolame metodu simulate
        foreach (Trans *trans, myTransList)
            running |= trans->simulate();
        step++;
    }
}

void pThread::xmlload(QByteArray data) {
    /**
     * @param data QByteArray, v němž je uložen xml kód s potřebnými informacemi
     * 
     * Načte strukturu Petriho sítě potřebnou pro simulaci do předem
     * pripravených datových struktur vlákna.
     */
    
    // Načítáme celou scénu znova, odstraníme tedy staré struktury 
    if(!myTransList.isEmpty()) {
        qDeleteAll(myTransList);
        myTransList.clear();
    }
    if(!myPlacesList.isEmpty()) {
        qDeleteAll(myPlacesList);
        myPlacesList.clear();
    }
    
    // Spojíme StreamReader s bytovým polem z argumentu
    QXmlStreamReader reader(data);
    
    int placesSize, transSize;  // bude se načítat počet míst a přechodů
    QXmlStreamAttributes currAttrs;     // použije se pro získání atributů z xml tagu
    QString attr;
    
    // Vektory pro snazší uchování informací o spojeních v přechodech
    vector< vector<sLink *> > transIn;
    vector< vector<sLink *> > transOut;
    
    
    reader.readNextStartElement();  // čte tag <petrisite ...>
    currAttrs = reader.attributes();    // získá počet míst a přechodů
    attr = currAttrs.value("transitions").toString();
    transSize = attr.toInt();
    
    attr = currAttrs.value("places").toString();
    placesSize = attr.toInt();
    
    attr = currAttrs.value("markedTrans").toString();
    marked = attr.toInt();
    
    int i = 0;
    while(i < transSize) {
        
        int x,y, places;
        QString watch, expr;
        
        Trans *newTrans = new Trans();
        
        while(!reader.readNextStartElement());      // čte <trans ...>
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        
        QPoint point(x, y);
        newTrans->setPos(point);
        
        reader.readNextStartElement();  // čte <watch>
        newTrans->setWatch(reader.readElementText());
        
        reader.readNextStartElement();  // čte <expr>
        newTrans->setExpr(reader.readElementText());
        
        myTransList.append(newTrans);
        
        reader.readNextStartElement();  // čte <in ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        int it = 0;
        vector<sLink *> inlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  //čte <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            inlinks.push_back(link);
            it++;
        }
        transIn.push_back(inlinks);
        
        while(!reader.readNextStartElement()); // čte <out ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        it = 0;
        vector<sLink *> outlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  // čte <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            outlinks.push_back(link);
            it++;
        }
        transOut.push_back(outlinks);
        i++;
    }
    
    i = 0;
    while(i < placesSize) {
        int x, y;
        
        while(!reader.readNextStartElement());  // čte <place ...>
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        QPoint point(x,y);
        
        Place *newPlace = new Place;
        newPlace->setPos(point);
        newPlace->setTokenString(currAttrs.value("tokens").toString());
        
        myPlacesList.append(newPlace);
        newPlace->updateTokens();
        
        i++;
    }
    
    // Zde proběhne spojení míst a přechodů za využití pomocných vektorů
    for(int i = 0; i < myTransList.size(); i++) {
        foreach(sLink *linkSym, transIn[i]) {
            Link *newLink = new Link(myPlacesList[linkSym->place], myTransList[i]);
            newLink->setLambda(linkSym->lambda);
        }
        foreach(sLink *linkSym, transOut[i]) {
            Link *newLink = new Link(myTransList[i], myPlacesList[linkSym->place]);
            newLink->setLambda(linkSym->lambda);

        }
    }
}

QByteArray pThread::xmlgen() {
    /**
     * Dovede ze současné situace v datových strukturách vlákna vygenerovat
     * xml strukturovaný soubor
     * 
     * @return Vrací QByteArray obsahující xml strukturu
     */
    QByteArray outs;    // návratový byte array
    QXmlStreamWriter writer(&outs);     // zapisujeme do outs

    writer.setAutoFormatting(true);
    writer.setCodec("utf-8");

    writer.writeStartDocument();
    writer.writeStartElement("petrisite");
    // Zapíšeme počet přechodů, míst a označený přechod (pokud není označen -> -1)
    writer.writeAttribute("transitions", QString::number(myTransList.size()));
    writer.writeAttribute("places", QString::number(myPlacesList.size()));
    writer.writeAttribute("markedTrans", QString::number(marked));
    
    for(int i = 0; i < myTransList.size(); i++) {
        writer.writeStartElement("trans");
        writer.writeAttribute("id", QString::number(i));
            writer.writeAttribute("x", QString::number(myTransList[i]->x(), 'g', 6));
            writer.writeAttribute("y", QString::number(myTransList[i]->y(), 'g', 6));
            
            writer.writeTextElement("watch", myTransList[i]->getWatch());
            writer.writeTextElement("expr", myTransList[i]->getExpr());
            
            writer.writeStartElement("in");
            writer.writeAttribute("num", QString::number(myTransList[i]->myInLinks.size()));
                for(int li = 0; li < myTransList[i]->myInLinks.size(); li++) {
                    writer.writeStartElement("link");
                    
                    int placeId = myPlacesList.indexOf((Place *)(myTransList[i]->myInLinks[li]->fromPnItem())); // fromPnItem coz link to trans goes from a place!!
                    writer.writeAttribute("place", QString::number(placeId));
                    
                    writer.writeTextElement("lambda", myTransList[i]->myInLinks[li]->getLambda());
                    
                    writer.writeEndElement();
                }
            writer.writeEndElement();
            
            writer.writeStartElement("out");
            writer.writeAttribute("num", QString::number(myTransList[i]->myOutLinks.size()));
                for(int li = 0; li < myTransList[i]->myOutLinks.size(); li++) {
                    writer.writeStartElement("link");
                    
                    int placeId = myPlacesList.indexOf((Place *)(myTransList[i]->myOutLinks[li]->toPnItem())); // fromPnItem coz link to trans goes to a place!!
                    writer.writeAttribute("place", QString::number(placeId));
                    
                    writer.writeTextElement("lambda", myTransList[i]->myOutLinks[li]->getLambda());
                    writer.writeEndElement();
                }
            writer.writeEndElement();
            
        writer.writeEndElement();
    }
    for(int i = 0; i < myPlacesList.size(); i++) {
        writer.writeStartElement("place");
        writer.writeAttribute("id", QString::number(i));
        writer.writeAttribute("tokens", myPlacesList[i]->getTokenString());
        writer.writeAttribute("x", QString::number(myPlacesList[i]->x(), 'g', 6));
        writer.writeAttribute("y", QString::number(myPlacesList[i]->y(), 'g', 6));
            
        writer.writeEndElement();
    }
    writer.writeEndElement();
    
    writer.writeEndDocument();
    
    return outs;
}
