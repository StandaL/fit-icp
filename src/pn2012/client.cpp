/**
 *  @file client.cpp
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *  
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 *
 *  Modul popisující připojování se ke vzdálenému serveru,
 *  popisuje též vytvoření dialogu pro zadání informací o serveru
 * 
 */

#include <QTcpSocket>
#include <QByteArray>
#include <QDebug>
#include <QApplication>
#include <QMessageBox>

#include <iostream>

#include "mainwindow.h"
#include "trans.h"
#include "client.h"

void mainWindow::connectToServer() {
    /**
     * Funkce pro připojení k serveru, po připojení odešle na server
     * data, která jsou potřebná pro simulaci.
     * V případě již existujícího připojení přechází rovnou k zasílání
     * dat.
     * Nebylo-li zatím zadáno jméno serveru, zobrazí se okno, kde je
     * možné jméno serveru a port upravit
     */
    // Pokud socket není připojen, zobrazíme okno pro připojení
    if(socket.state() == QAbstractSocket::UnconnectedState) {
        serverDialog *newDial = new serverDialog(this);
        newDial->show();
        newDial->raise();
        newDial->activateWindow();
        connect(newDial, SIGNAL(setServer(QString)), this, SLOT(setServer(QString)));
        connect(newDial, SIGNAL(setPort(int)), this, SLOT(setPort(int)));
        connect(newDial, SIGNAL(getSimData()), this, SLOT(getSimData()));
    }
    else getSimData();  // jinak přejdeme přímo k získání informací
}

void mainWindow::disconnectFromServer() {
    /**
     * Slot funkce pro odpojení socketu
     */
    socket.disconnectFromHost();
}

void mainWindow::getSimData() {
    /**
     * Tato funkce zašle na server data ze scény převedená do xml podoby.
     * Server poté provede simulaci a zašle přepočítaná data zpět.
     * V případě ukončení komunikace jsou dokončeny read/write přenosy
     * a poté je socket odpojen.
     */
     
    // Došlo-li by k odpojení socketu ze strany serveru, ukonči spojení
    connect(&socket, SIGNAL(disconnected()), this, SLOT(disconnectFromServer()));
    
    // Pokud socket není připojen, pokusíme se o připojení (případ volání této
    // funkce z serverDialog
    if(socket.state() != QAbstractSocket::ConnectedState) {
        socket.connectToHost(server, port);
        if(!socket.waitForConnected()) {
            QMessageBox fail;
            fail.setText("Connection failed.");
            fail.setIcon(QMessageBox::Warning);
            fail.exec();
            return;
        }
    }
    // Je-li socket připojen, proběhne generování xml a zašle se na server
    if(socket.state() == QAbstractSocket::ConnectedState) {
        QString data = xmlgen();
        socket.flush();
        socket.write((const char *)data.toLatin1(), data.size());
        if(!socket.waitForReadyRead()) {
            QMessageBox fail;
            fail.setText("Connection failed.");
            fail.setIcon(QMessageBox::Warning);
            fail.exec();
            return;
        }
        
        QByteArray datain;
        datain = socket.readAll();  // Získání dat ze serveru
        
        // Vyžádá-li si server přihlašovací údaje, otevře se okno pro jejich
        // dotázání
        if(datain == QByteArray("whoareu")) {
            authDialog *newDial = new authDialog(this);
            newDial->show();
            newDial->raise();
            newDial->activateWindow();
            connect(newDial, SIGNAL(setLogin(QString)), this, SLOT(setLogin(QString)));
            connect(newDial, SIGNAL(setPasswd(QString)), this, SLOT(setPasswd(QString)));
            connect(newDial, SIGNAL(sendAuth()), this, SLOT(sendAuth()));
        }
        else {
            // Jsme-li již přihlášeni, načteme získaná data
            xmlload(datain);
            if(marked > -1) {
                myTransList[marked]->setSelected(true);
            }
        }
    }
}

serverDialog::serverDialog(QWidget *parent) : QDialog(parent) {
    /**
     * Konstruktor dialogu pro získání informací o serveru. Zajistí
     * načtení jména serveru, jeho portu, a následně spustí signály,
     * které vyústí v pokus o připojení.
     */
     
    label = new QLabel("Zadejte název serveru a port: ");
    servLineEdit = new QLineEdit("localhost");
    portLineEdit = new QLineEdit("8080");
    acceptButton = new QPushButton("OK");
    closeButton = new QPushButton("Cancel");
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(servLineEdit);
    mainLayout->addWidget(portLineEdit);
    
    QVBoxLayout *buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(acceptButton);
    buttonLayout->addWidget(closeButton);
    
    mainLayout->addLayout(buttonLayout);
    
    setLayout(mainLayout);
}

void serverDialog::acceptSlot() {
    /**
     * Vyvolá signály pro zápis do jména serveru a portu, následně
     * signál pro pokus o připojení.
     */
    emit setServer(servLineEdit->text());
    emit setPort(portLineEdit->text().toInt());
    emit close();
    emit getSimData();
}

void mainWindow::setServer(QString str) {
    /** Nastaví jméno serveru */
    server = str;
}

void mainWindow::setPort(int p) {
    /** Nastaví číslo portu */
    port = p;
}

authDialog::authDialog(QWidget *parent) : QDialog(parent) {
    /**
     * Konstruktor dialogu pro získání informací o serveru. Zajistí
     * načtení jména serveru, jeho portu, a následně spustí signály,
     * které vyústí v pokus o připojení.
     */
    label = new QLabel("Login a heslo ");
    loginLineEdit = new QLineEdit("");
    passwdLineEdit = new QLineEdit("");
    passwdLineEdit->setEchoMode(QLineEdit::Password);
    acceptButton = new QPushButton("OK");
    closeButton = new QPushButton("Cancel");
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(loginLineEdit);
    mainLayout->addWidget(passwdLineEdit);
    
    QVBoxLayout *buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(acceptButton);
    buttonLayout->addWidget(closeButton);
    
    mainLayout->addLayout(buttonLayout);
    
    setLayout(mainLayout);
}

void authDialog::acceptSlot() {
    /**
     * Vyvolá signály pro zápis do jména serveru a portu, následně
     * signál pro pokus o připojení.
     */
    emit setLogin(loginLineEdit->text());
    emit setPasswd(passwdLineEdit->text());
    emit sendAuth();
    emit close();
}

void mainWindow::setLogin(QString l) {
    /**
     * @param l QString obsahující login
     * 
     * Zapíše login do proměnné objektu
     */
    login = l;
}

void mainWindow::setPasswd(QString p) {
    /**
     * @param p QString obsahující heslo
     * 
     * Zapíše heslo do proměnné objektu
     */
    passwd = p;
}

void mainWindow::sendAuth() {
    /**
     * Odeslání autentifikačních údajů na server, očekává i výsledek výpočtu
     * ze serveru
     */
     
    // Zašle heslo na server ve tvaru login:heslo
    QString auth = login+':'+passwd;
    socket.flush();
    socket.write((const char *)auth.toLatin1(), auth.size());
    QByteArray datain;
    if(!socket.waitForReadyRead())
        return;
    datain = socket.readAll();
    xmlload(datain);
    // Označit po načtení opět ten samý přechod!
    if(marked > -1) {
        myTransList[marked]->setSelected(true);
    }
}
