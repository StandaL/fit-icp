/**
 *  @file  xmlhandler.cpp
 *  @author  Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Modul pro nahravani a zapisovani siti ve formatu XML.
 *  
 */

#include "xmlhandler.h"

using namespace std;


void mainWindow::setFilename(QString name) {
    /**
     * @param name Jméno kýženého souboru
     * 
     * Nataví jméno souboru, s kterým pracujeme
     */
    filename = name;
}

void mainWindow::saveAsXml() {
    /**
     * Implementace chování "Uložit jako...", zeptá se vždy na jméno souboru
     */
    QString fileName = QFileDialog::getSaveFileName(this,
    "Uloz soubor", ".", "Soubory XML (*.xml)");
    if (fileName.isEmpty())
        return;
    setFilename(fileName);
    QFile fw(filename);
    if(!fw.open(QIODevice::WriteOnly)) {
        cerr << "Unable to open file.\n";
        return;
    }
    QTextStream xml(&fw);
    int tempmarked = marked;
    xml << xmlgen();
    marked = tempmarked;
    fw.close();
}

void mainWindow::saveXml() {
    /**
     * Implementace "Uložit", zeptá se na jméno souboru pouze, pokud s žádným
     * zatím nepracujeme
     */
    if(filename.isEmpty()) {
        QString fileName = QFileDialog::getSaveFileName(this,
        "Uloz soubor", ".", "Soubory XML (*.xml)");
        if (fileName.isEmpty())
            return;
        setFilename(fileName);
    }
    QFile fw(filename);
    if(!fw.open(QIODevice::WriteOnly)) {
        cerr << "Unable to open file.\n";
        return;
    }
    QTextStream xml(&fw);
    int tempmarked = marked;
    xml << xmlgen();
    marked = tempmarked;
    fw.close();
}

void mainWindow::openXml() {
    /**
     * Implementace pro klasické "Otevřít"
     */
    QString fileName = QFileDialog::getOpenFileName(this,
        "Otevri soubor", ".", "Soubory XML (*.xml)");
    setFilename(fileName);
    xmlload();
}

/*!
 * @brief Pomocná struktura pro uložení informace o spojeních při načítání xml
 */
typedef struct sLink {
    int place;  /*!< Místo, s nímž je přechod spojen */
    QString lambda; /*!< Lambda výraz na spojení */
} sLink;

QString mainWindow::xmlgen() {
    /**
     * Dovede ze současné situace v datových strukturách vlákna vygenerovat
     * xml strukturovaný soubor
     * 
     * @return Vrací QByteArray obsahující xml strukturu
     */
    QString outs;   // návratový byte array
    QXmlStreamWriter writer(&outs);     // zapisujeme do outs

    writer.setAutoFormatting(true);
    writer.setCodec("utf-8");

    writer.writeStartDocument();
    writer.writeStartElement("petrisite");
    // Zapíšeme počet přechodů, míst a označený přechod (pokud není označen -> -1)
    writer.writeAttribute("transitions", QString::number(myTransList.size()));
    writer.writeAttribute("places", QString::number(myPlacesList.size()));
    writer.writeAttribute("markedTrans", QString::number(marked));
    
    for(int i = 0; i < myTransList.size(); i++) {
        writer.writeStartElement("trans");
        writer.writeAttribute("id", QString::number(i));
            writer.writeAttribute("x", QString::number(myTransList[i]->x(), 'g', 6));
            writer.writeAttribute("y", QString::number(myTransList[i]->y(), 'g', 6));
            
            writer.writeTextElement("watch", myTransList[i]->getWatch());
            writer.writeTextElement("expr", myTransList[i]->getExpr());
            
            writer.writeStartElement("in");
            writer.writeAttribute("num", QString::number(myTransList[i]->myInLinks.size()));
                for(int li = 0; li < myTransList[i]->myInLinks.size(); li++) {
                    writer.writeStartElement("link");
                    
                    int placeId = myPlacesList.indexOf((Place *)(myTransList[i]->myInLinks[li]->fromPnItem())); // fromPnItem protože link do trans jde z místa!!
                    writer.writeAttribute("place", QString::number(placeId));
                    
                    writer.writeTextElement("lambda", myTransList[i]->myInLinks[li]->getLambda());
                    
                    writer.writeEndElement();
                }
            writer.writeEndElement();
            
            writer.writeStartElement("out");
            writer.writeAttribute("num", QString::number(myTransList[i]->myOutLinks.size()));
                for(int li = 0; li < myTransList[i]->myOutLinks.size(); li++) {
                    writer.writeStartElement("link");
                    
                    int placeId = myPlacesList.indexOf((Place *)(myTransList[i]->myOutLinks[li]->toPnItem()));
                    writer.writeAttribute("place", QString::number(placeId));
                    
                    writer.writeTextElement("lambda", myTransList[i]->myOutLinks[li]->getLambda());
                    writer.writeEndElement();
                }
            writer.writeEndElement();
            
        writer.writeEndElement();
    }
    for(int i = 0; i < myPlacesList.size(); i++) {
        writer.writeStartElement("place");
        writer.writeAttribute("id", QString::number(i));
        writer.writeAttribute("tokens", myPlacesList[i]->getTokenString());
        writer.writeAttribute("x", QString::number(myPlacesList[i]->x(), 'g', 6));
        writer.writeAttribute("y", QString::number(myPlacesList[i]->y(), 'g', 6));
            
        writer.writeEndElement();
    }
    writer.writeEndElement();
    
    writer.writeEndDocument();
    
    return outs;
}

void mainWindow::xmlload() {
    /**
     * Načte strukturu Petriho sítě potřebnou pro simulaci do předem
     * pripravených datových struktur vlákna.
     */
    if(!myTransList.isEmpty()) {
        qDeleteAll(myTransList);
        myTransList.clear();
    }
    if(!myPlacesList.isEmpty()) {
        qDeleteAll(myPlacesList);
        myPlacesList.clear();
    }
    scene->clear();
    if (filename.isEmpty())
        return;
    QFile xmlfile(filename);
    xmlfile.open(QIODevice::ReadOnly);
    
    QXmlStreamReader reader(&xmlfile);
    
    int placesSize, transSize;
    QXmlStreamAttributes currAttrs;
    QStringRef attrRef;
    QString attr;
    vector< vector<sLink *> > transIn;
    vector< vector<sLink *> > transOut;
    
    
    reader.readNextStartElement();  // reads <petrisite ...>
    currAttrs = reader.attributes();    // gets number of places and transitions
    attr = currAttrs.value("transitions").toString();
    transSize = attr.toInt();
    
    attr = currAttrs.value("places").toString();
    placesSize = attr.toInt();
    
    attr = currAttrs.value("markedTrans").toString();
    marked = attr.toInt();
    
    int i = 0;
    while(i < transSize) {
        
        int x,y, places;
        QString watch, expr;
        
        Trans *newTrans = new Trans(this);
        
        while(!reader.readNextStartElement());      // reads transition
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        
        QPoint point(x, y);
        
        reader.readNextStartElement();  // reads <watch>
        newTrans->setWatch(reader.readElementText());
        
        reader.readNextStartElement();  // reads <expr>
        newTrans->setExpr(reader.readElementText());
        
        newTrans->setPos(point);
        scene->addItem(newTrans);
        myTransList.append(newTrans);
        
        reader.readNextStartElement();  // reads <in ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        int it = 0;
        vector<sLink *> inlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  //reads <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            inlinks.push_back(link);
            // for qt returns to root elem, this is how we get there so we are
            // able to read next element
            it++;
        }
        transIn.push_back(inlinks);
        
        while(!reader.readNextStartElement()); // reads <out ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        it = 0;
        vector<sLink *> outlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  //reads <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            outlinks.push_back(link);
            it++;
        }
        transOut.push_back(outlinks);
        i++;
    }
    
    i = 0;
    while(i < placesSize) {
        int x, y;
        
        while(!reader.readNextStartElement());
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        QPoint point(x,y);
        
        Place *newPlace = new Place(this);        
        newPlace->setTokenString(currAttrs.value("tokens").toString());
        
        newPlace->setPos(point);
        scene->addItem(newPlace);
        myPlacesList.append(newPlace);
        newPlace->updateTokens();
        
        i++;
    }
    
    for(int i = 0; i < myTransList.size(); i++) {
        foreach(sLink *linkSym, transIn[i]) {
            Link *newLink = new Link(myPlacesList[linkSym->place], myTransList[i]);
            newLink->setLambda(linkSym->lambda);
            scene->addItem(newLink);
        }
        foreach(sLink *linkSym, transOut[i]) {
            Link *newLink = new Link(myTransList[i], myPlacesList[linkSym->place]);
            newLink->setLambda(linkSym->lambda);
            scene->addItem(newLink);

        }
    }
    
    xmlfile.close();
}

void mainWindow::xmlload(QByteArray data) {
    /**
     * @param data QByteArray, v němž je uložen xml kód s potřebnými informacemi
     * 
     * Přetížení xmlload tak, aby byla usnadněna práce při načtení dat ze socketu
     */
    if(!myTransList.isEmpty()) {
        qDeleteAll(myTransList);
        myTransList.clear();
    }
    if(!myPlacesList.isEmpty()) {
        qDeleteAll(myPlacesList);
        myPlacesList.clear();
    }
    scene->clear();
    
    QXmlStreamReader reader(data);
    
    int placesSize, transSize;
    QXmlStreamAttributes currAttrs;
    QStringRef attrRef;
    QString attr;
    vector< vector<sLink *> > transIn;
    vector< vector<sLink *> > transOut;
    
    
    reader.readNextStartElement();  // čte <petrisite ...>
    currAttrs = reader.attributes();    // získá počet míst a přechodů
    attr = currAttrs.value("transitions").toString();
    transSize = attr.toInt();
    
    attr = currAttrs.value("places").toString();
    placesSize = attr.toInt();
    
    attr = currAttrs.value("markedTrans").toString();
    marked = attr.toInt();
    
    int i = 0;
    while(i < transSize) {
        
        int x,y, places;
        QString watch, expr;
        
        Trans *newTrans = new Trans(this);
        
        while(!reader.readNextStartElement());      // čte <trans ...>
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        
        QPoint point(x, y);
        
        reader.readNextStartElement();  // čte <watch>
        newTrans->setWatch(reader.readElementText());
        
        reader.readNextStartElement();  // čte <expr>
        newTrans->setExpr(reader.readElementText());
        
        newTrans->setPos(point);
        scene->addItem(newTrans);
        myTransList.append(newTrans);
        
        reader.readNextStartElement();  // čte <in ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        int it = 0;
        vector<sLink *> inlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  // čte <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            inlinks.push_back(link);
            it++;
        }
        transIn.push_back(inlinks);
        
        while(!reader.readNextStartElement()); // čte <out ...>
        currAttrs = reader.attributes();
        places = currAttrs.value("num").toString().toInt();
        it = 0;
        vector<sLink *> outlinks;
        while(it < places) {
            sLink *link = new sLink;
            
            while(!reader.readNextStartElement());  // čte <link ...>
            currAttrs = reader.attributes();
            link->place = currAttrs.value("place").toString().toInt();
            reader.readNextStartElement();
            link->lambda = reader.readElementText();
            outlinks.push_back(link);
            it++;
        }
        transOut.push_back(outlinks);
        i++;
    }
    
    i = 0;
    while(i < placesSize) {
        int x, y;
        
        while(!reader.readNextStartElement());  // čte <place ...>
        currAttrs = reader.attributes();
        x = currAttrs.value("x").toString().toInt();
        y = currAttrs.value("y").toString().toInt();
        QPoint point(x,y);
        
        Place *newPlace = new Place(this);        
        newPlace->setTokenString(currAttrs.value("tokens").toString());
        
        newPlace->setPos(point);
        scene->addItem(newPlace);
        myPlacesList.append(newPlace);
        newPlace->updateTokens();
        
        i++;
    }
    
    // Uskuteční spojení mezi místy a přechody
    for(int i = 0; i < myTransList.size(); i++) {
        foreach(sLink *linkSym, transIn[i]) {
            Link *newLink = new Link(myPlacesList[linkSym->place], myTransList[i]);
            newLink->setLambda(linkSym->lambda);
            scene->addItem(newLink);
        }
        foreach(sLink *linkSym, transOut[i]) {
            Link *newLink = new Link(myTransList[i], myPlacesList[linkSym->place]);
            newLink->setLambda(linkSym->lambda);
            scene->addItem(newLink);

        }
    }
    
}
