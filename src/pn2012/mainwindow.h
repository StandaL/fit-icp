/**
 *  @file   mainwindow.h
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Definice trid pro praci s hlavnim oknem.
 * 
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPair>
#include <QActionGroup>
#include <QTcpSocket>
#include <QByteArray>

#include "scene.h"
#include "client.h"

class QGraphicsScene;
class QGraphicsView;
class Trans;
class Place;
class Link;

/*!
 * @brief Třída hlavního okna
 * 
 * Tato třída je hlavní třídou pro tvorbu rozhraní a jeho komunikaci s dalšími
 * moduly - vytváření přechodů, míst, spojení, komunikace se serverem apod.
 */
class mainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    mainWindow();

public slots:
    void createItem(myItems sel, QPoint point);
    void newScene();
    void setFilename(QString);
    void setServer(QString);
    void setPort(int);
    void setLogin(QString);
    void setPasswd(QString);
    
    QString xmlgen();

private slots:
    void xmlload();
    void xmlload(QByteArray);
    void connectToServer();
    void disconnectFromServer();
    void saveXml();
    void saveAsXml();
    void openXml();
    void getSimData();
    void deleteItems();
    void sendAuth();
    
    void addTrans(Trans * newTrans);
    void addPlace(Place * newPlace);
    void addLink(Link* newLink);
    
    void simTillEnd();
    void step();

private:
    void createActions();
    void createMenu();
    void createStatusBar();
    void createToolBars();
    
    // Následuje výpis některých důležitých objektů pro vykreslení GUI
    QMenu *fileMenu;
    QActionGroup* itemGroup;
    QAction *newAction;
    QAction *openAction;
    QAction *saveAction;
    QAction *saveAsAction;
    QAction *exitAction;
    QAction *itemPlaceAction;
    QAction *itemMoveAction;
    QAction *itemLinkAction;
    QAction *itemTransAction;
    QAction *fullSimAction;
    QAction *disconnectAction;
    QAction *reloadAction;
    QAction *markAction;
    QToolBar *fileToolBar;
    QToolBar *itemToolBar;  
    
    // Tyto proměnné jsou důležité pro komunikaci se serverem
    int port;
    QString server;
    QTcpSocket socket;
    QString login;
    QString passwd;
    
    QString filename;
    
    // Identifikátor přechodu označeného pro krokování
    int marked;
    
    QGraphicsScene *scene;
    QGraphicsView *view;
    
    QList<Trans *> myTransList;
    QList<Place *> myPlacesList;
};

#endif
