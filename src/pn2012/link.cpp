/**
 *  @file  link.cpp
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 *
 *  @note Žádný kód z tohoto modulu nebyl vygenerován automaticky.
 *          
 *  Modul popisující chování hran v síti.
 * 
 */


#include <QtGui>
#include <iostream>

#include "link.h"
#include "place.h"
#include "pnItem.h"

// definice citace
int Link::counter = 0;

Link::Link(PnItem *fromPnItem, PnItem *toPnItem)
{
    
    // promenne pro zachovani informace o tom, z jakeho a do jakeho uzlu vedou
    myFromPnItem = fromPnItem;
    myToPnItem = toPnItem;
    
    // pridani informace o spoji do uzlu
    myFromPnItem->addOutLink(this);
    myToPnItem->addInLink(this);

    // nastaveni flagu pro vyber itemu
    setFlags(QGraphicsItem::ItemIsSelectable);
    // kreslime na pozadi
    setZValue(-1);

    // nastavime barvu a zavolame meto pro vytvoreni spoje mezi body
    setColor(Qt::black);
    trackNodes();
}

// destruktor
Link::~Link()
{
    if(myFromPnItem != NULL) myFromPnItem->removeOutLink(this);
    if(myToPnItem != NULL) myToPnItem->removeInLink(this);
}

void Link::setFromPnItem(PnItem *item) {
    myFromPnItem = item; 
}

void Link::setToPnItem(PnItem *item) {
    myToPnItem = item;
}

// metoda pro update stavu
void Link::update() {
    // nejdrive vyprazdnime seznam
    qDeleteAll(itemList);
    itemList.clear();
    // vytvorime regexp
    QRegExp rx1("(\\w+)'?(\\w*)");
    int pos = 0;

    // projedeme cely vstupni string a najdeme vsechny vyskyty lambda vyrazu
    while ((pos = rx1.indexIn(linkLambda, pos)) != -1) {
        // vytvorime novy LinkItem, inicializujeme ho a vlozime do seznamu
        LinkItem *tempItem;
        if (!rx1.cap(2).isEmpty())
			tempItem = new LinkItem(rx1.cap(1), rx1.cap(2));
		else
			tempItem = new LinkItem(rx1.cap(1), "1");
        itemList.append(tempItem);
        pos += rx1.matchedLength();
    }
	// nakonec resetujeme iterator u mista
	Place * place = dynamic_cast<Place*>(myFromPnItem);
	if (!place)
		place = dynamic_cast<Place*>(myToPnItem);
	place->resetIter();
}

// metoda pro otestovani, zda je v mistech potrebny token
bool Link::checkConst() {
	Place * place = dynamic_cast<Place*>(myFromPnItem);
	if (place->getToken(itemList[0]->getVarName().toInt(), itemList[0]->getTokenNum().toInt()))
		return true;
	else return false;
}

// metoda pro ziskani dalsi hodnoty tokenu
int Link::getNextVal() {
	Place * place = dynamic_cast<Place*>(myFromPnItem);
	// vezmeme dalsi token
	int nextToken = place->getToken(itemList[0]->getTokenNum().toInt());
	// pokud neni, ohlasime, ze jsme prosli vsechny tokeny a vygenerujeme dalsi od zacatku
	if (nextToken == -1) {
		emit overflow(this);
		return place->getToken(itemList[0]->getTokenNum().toInt());
	}
	// jinak vratime token
	else return nextToken; 
}

void Link::removeToken(int tokenVal) {
	Place * place = dynamic_cast<Place*>(myFromPnItem);
	for (int i = 0; i < itemList[0]->getTokenNum().toInt(); i++)
		place->removeToken(tokenVal);
}

void Link::addToken(int tokenVal) {
	Place * place = dynamic_cast<Place*>(myToPnItem);
	for (int i = 0; i < itemList[0]->getTokenNum().toInt(); i++)
		place->addToken(tokenVal);
}

// metoda pro pristup k fromPnItem
PnItem *Link::fromPnItem() const
{
    return myFromPnItem;
}

// metoda pro pristup k toPnItem
PnItem *Link::toPnItem() const
{
    return myToPnItem;
}

QList<LinkItem *> Link::getItemList() {
	return itemList;
}

QString Link::getVar() {
	return itemList[0]->getVarName();
}

// metoda pro nastaveni barvy
void Link::setColor(const QColor &color)
{
    setPen(QPen(color, 1.0));
}

// metoda pro vytvoreni spoje mezi body danymi pozici PnItemu
void Link::trackNodes()
{
    setLine(QLineF(myFromPnItem->pos(), myToPnItem->pos()));
}

// redefinice mtedoy pro vykresleni spoje
void Link::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QBrush brush(Qt::black);
    // nejdrive zavolame metodu z QGraphicsLineItem, ktera nam vykresli primku
    QGraphicsLineItem::paint(painter, option, widget);
    // upravime velikost fontu
    painter->setFont(QFont(painter->font().family(), 14));
    // a vypiseme ID spoje
    painter->drawText((line().p1() + line().p2())/2, linkLambda);
    painter->setBrush(brush);	
    painter->drawEllipse((line().p1() + line().p2())/4 + line().p2()/2, 3, 3);
}

// metoda pro nastaveni ID spoje
void Link::setLambda(QString newLambda) {
    linkLambda = newLambda;
}

QString Link::getLambda() const
{
    return linkLambda;
}

// LinkItem
LinkItem::LinkItem(QString name, QString num) {
    varName = name;
    tokenNum = num;
}

void LinkItem::setVarName(QString newName) {
    varName = newName;
}

void LinkItem::setTokenNum(QString newNum) {
    tokenNum = newNum;
}

QString LinkItem::getVarName() {
    return varName;
}

QString LinkItem::getTokenNum() {
    return tokenNum;
}


LineEditDialog::LineEditDialog(QWidget *parent) : QDialog(parent) {
    label = new QLabel("Promenna/konstanta");
    lambdaLineEdit = new QLineEdit("lambda");
    acceptButton = new QPushButton("OK");
    closeButton = new QPushButton("Cancel");
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(lambdaLineEdit);
    
    QVBoxLayout *buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(acceptButton);
    buttonLayout->addWidget(closeButton);
    
    mainLayout->addLayout(buttonLayout);
    
    setLayout(mainLayout);
}

void LineEditDialog::acceptSlot() {
    emit setLambda(lambdaLineEdit->text());
    emit close();
}
