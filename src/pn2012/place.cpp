/**
 *  @file    place.cpp
 *  @author  Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Modul popisujici chovani mist v siti.
 *   
 */

#include <QtGui>
#include <iostream>

#include "place.h"
#include "pnItem.h"

// konstruktor
Place::Place(QWidget *parent) {
    /**
     * @param parent QWidget, s kterým bude spojeno zobrazení místa
     * 
     * Vytvoří místo a nastaví k němu tokeny podle spuštěného dialogu
     */
    currIter = 0;
    // dulezite flagy - pohyb, selekce a zjisteni zmeny polohy
    setFlags(ItemIsMovable | ItemIsSelectable | ItemSendsGeometryChanges);

    placeDialog = new PlaceEditDialog(this);
    connect(placeDialog, SIGNAL(addItem(Place *)), parent, SLOT(addPlace(Place *)));
}

Place::Place() {
    /**
     * Přetížený konstruktor pro využití na místech bez GUI (server)
     */
    placeDialog = NULL;
    currIter = 0;
}

Place::~Place() {
    if (placeDialog)
        delete placeDialog;
}

void Place::showDialog() {
    /**
     * Zobrazí dialog místa 
     */
    placeDialog->show();
    placeDialog->raise();
    placeDialog->activateWindow();
}

void Place::paint(QPainter *painter,
                 const QStyleOptionGraphicsItem *option,
                 QWidget *)
{
    /**
     * Předefinovaná funkce pro vykreslení objektu, nastavuje barvu a podobné
     * grafické vlastnosti objektu
     */
    // Barva obrysu
    QPen pen(Qt::darkBlue);
    // Pokud je misto vybrane, vykreslime obrys carkovane
    if (option->state & QStyle::State_Selected)
        pen.setStyle(Qt::DotLine);
    // nastavime parametru
    painter->setPen(pen);
    painter->setBrush(Qt::white);
    // a vykreslime
    painter->drawEllipse(-25,-25, 50, 50);
    painter->drawText(-25, 40, tokenString);
}

QRectF Place::boundingRect() const
{
    /**
     * Předefinovaná funkce pro velikost objektu
     * 
     * @return Vrací upravený grafický objekt
     */
    const int Margin = 1;
    // objekt plus pixel
    return outlineRect().adjusted(-Margin, -Margin, +Margin, +Margin); // <-- nenapsano obecne, zmenit!
}

QRectF Place::outlineRect() const
{
    /**
     * Přizpůsobení prostoru pro objekt jeho obsahu
     * 
     * @return Vrací upravený grafický objekt
     */
    // zjistime metriky pouzivane kodu
    QFontMetricsF metrics = qApp->font();
    // a vytvorime obdelniky okolo vstupniho textu
    QRectF rect = metrics.boundingRect(tokenString);
    // zjistime, jestli je text delsi nez obdelnik
    if (rect.width() < 50)
        // vratime default obdelnik zvetseny o 16px 
        return QRectF(-25, -40, 50, 80);
    else {
        // pouzijeme sirsi obdelnik
        return QRectF(-25, -40, rect.width(), 80);
    }
}

void Place::setTokenString(QString newString) {
    /**
     * @param newString Nový řetězec tokenů místa
     * 
     * Nastaví řetězec tokenů danému místu
     */
    tokenString = newString;
}

QString Place::getTokenString() {
    /**
     * Vrací řetězec tokenů daného místa
     */
    return tokenString;
}

void Place::updateString() {
    /**
     * Upraví string místa podle tokenů v seznamu tokenů
     */
    QString newString("");
    foreach (PnToken * token, tokenList) {
        for (int i = 0; i < token->getTokenNum(); i++) {
            newString += QString::number(token->getTokenID()) + ", ";
        }
    }
    newString.chop(2);
    tokenString = newString;
    update();
}

void Place::updateTokens(){
    /**
     * Upraví tokeny podle stringu v daném místě
     */
    qDeleteAll(tokenList);
    tokenList.clear();
    
    QRegExp rx("(\\d+),?\\s*");
    int pos = 0;
    int found = 0;
    while ((pos = rx.indexIn(tokenString, pos)) != -1) {
        PnToken * tempToken = new PnToken(rx.cap(1).toInt(),1);
        foreach (PnToken * token, tokenList) {
            if (*token == *tempToken) {
                token->setTokenNum(token->getTokenNum()+1);
                found = 1;
                break;
            }
        }
        if (!found) {
            tokenList.append(tempToken);
        }
        pos += rx.matchedLength();
        found = 0;
    }
}  
// metoda pro ziskani tokenu
int Place::getToken(int count) {
    /**
     * @param count Pocet tokenu, ktery je potreba namapovat.
     * 
     * Metoda pro ziskani informace o dalsim moznem namapovani tokenu.
     * 
     * @return Hodnota tokenu, ktera se namapuje do promenne.
     * @return -1 pokud nenalezne vhodny token
     */
    // pokud jsme prosli vsechno, prochazime znova
    if (currIter == tokenList.size())
        currIter = 0;
    for (int i = currIter; i < tokenList.size(); i++) {
        currIter++;
        // pokud se nasel vhodny token, vratime ho
        if (tokenList[i]->getTokenNum() >= count)
            return tokenList[i]->getTokenID();
    }
    // pokud ne, vracime -1
    return -1;
}

// pretizeni metody pro ziskani tokenu
int Place::getToken(int value, int count) {
    /**
     * @param value Hodnota potrebneho tokenu.
     * @param count Pocet potrebnych tokenu.
     * 
     * Metoda pro ziskani informace o tom, jestli je v miste potrebny pocet
     * tokenu potrebne hodnoty.
     * 
     * @return 1 Pokud jsou tokeny k dispozici.
     * @return 0 Pokud nejsou tokeny k dispozici.
     */
    // pokud jsme prosli vsechno, prochazime znova
    if (currIter == tokenList.size())
        currIter = 0;
    for (int i = currIter; i < tokenList.size(); i++) {
        currIter++;
        // pokud mame dostatek tokenu spravne hodnoty
        if ((tokenList[i]->getTokenID() == value) && (tokenList[i]->getTokenNum() >= count))
            return 1;
    }
    // pokud ne, vracime 0
    return 0;
}

int Place::getListSize() {
    /**
     * Vrátí velikost seznamu tokenů
     */
    return tokenList.size();
}

void Place::resetIter() {
    /**
     * Navrací iterátor míst na 0
     */
    currIter = 0;
}

void Place::removeToken(int tokenVal) {
    /**
     * @param tokenVal Token, který má být odebrán
     * 
     * Odebere token o hodnotě tokenVal ze seznamu tokenů
     */
    foreach (PnToken * token, tokenList)
        if (token->getTokenID() == tokenVal)
            token->setTokenNum(token->getTokenNum()-1);
    updateString();
}

void Place::addToken(int tokenVal) {
    /**
     * @param tokenVal Nový token
     * 
     * Přidá token o zadané hodnotě do seznamu tokenů
     */
    int found = 0;
    foreach (PnToken * token, tokenList)
        if (token->getTokenID() == tokenVal) {
            token->setTokenNum(token->getTokenNum()+1);
            found++;
        }
    if (!found) {
        PnToken * tempToken = new PnToken(tokenVal, 1);
        tokenList.append(tempToken);
    }
    updateString();
}

void Place::showEditDialog() {
	/**
     * Metoda pro zavolani dialogu na editaci
     */
	placeDialog->editDialog();
	showDialog();
}

void Place::mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event ) {
	/**
     * Redefinice metody pro odchyceni dvojiteho poklepani na item
     * Spusti editacni dialog
     * \param event Odchyceny event.
     */
	if(event->button() == Qt::LeftButton)
		showEditDialog();
	// propagujeme dale
	QGraphicsObject::mouseDoubleClickEvent(event);
}


// PlaceEditDialog
PlaceEditDialog::PlaceEditDialog(Place * newPlace, QWidget *parent) : QDialog(parent) {
    /**
     * @param newPlace Ukazatel na místo, jež dialog upravuje
     * @param parent Widget, s kterým bude tento dialog spojen
     * 
     * Konstruktor dialogu pro úpravu míst
     */
    label = new QLabel("Tokeny:");
    tokenLineEdit = new QLineEdit("");
    acceptButton = new QPushButton("OK");
    closeButton = new QPushButton("Cancel");
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(tokenLineEdit);
    
    QVBoxLayout *buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(acceptButton);
    buttonLayout->addWidget(closeButton);
    
    mainLayout->addLayout(buttonLayout);
    
    setLayout(mainLayout);
    
    place = newPlace;
}

void PlaceEditDialog::editDialog() {
	/**
     * Metoda, ktera pripravi dialog na editaci.
     */
	tokenLineEdit->clear();
	tokenLineEdit->insert(place->getTokenString());
	acceptButton->disconnect();
	connect(acceptButton, SIGNAL(clicked()), this, SLOT(editSlot()));
	
}

void PlaceEditDialog::acceptSlot() {
    /**
     * Slot pro akci po kliknutí na tlačítko OK, zmení token string místa
     * a vytvoří signál na přidání místa do scény
     */
    place->setTokenString(tokenLineEdit->text());
    emit addItem(place);
    emit close();
}

void PlaceEditDialog::editSlot() {
	/**
     * Slot, ktery při kliknutí na OK v dialogu zmeni retezce straze a vyrazu
     */
	place->setTokenString(tokenLineEdit->text());
	place->updateTokens();
	place->updateString();
    emit close();
}
