/**
	Autori:	Petr Kubat, xkubat11@stud.fit.vutbr.cz
	 		Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
	Modul: 	main.cpp
	Popis:	Modul pro spusteni vlastni aplikace.
	
	Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
*/


#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
	//Main, co dodat.
	QApplication app(argc, argv);
	mainWindow mainWin;
	mainWin.show();
	app.exec();
}
