/**
 *  @file trans.h
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 *  
 *  Obsahuje definice trid pro praci s prechody.
 *   
 */
 
#ifndef TRANS_H
#define TRANS_H

#include <QApplication>
#include <QGraphicsItem>
#include <QDialog>

#include "pnItem.h"

class QLineEdit;
class QLabel;
class QPushButton;

class WatchItem;
class BoundVarItem;
class TransEditDialog;

/*!
 * Enumerační typ pro určení operátorů v třídě watchItem
 */
enum myOps {OLOW, OLEQ, OGEQ, OGRT, OEQ, ONEQ};

/*!
 * @brief Třída přechodů
 * 
 * Třída pro objekty přechodů, zahrnuje definici "watch" a "expr" výrazů
 * pro výpočet nad Petriho sítí
 */
class Trans : public PnItem
{
    Q_OBJECT
public:
    
    Trans();
    Trans(QWidget *parent);
    ~Trans();
    
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    QRectF outlineRect() const;
    
    QString getWatch();
    QString getExpr();
    
    void showDialog();
    void showEditDialog();
    
    int simulate();
    void parseWatch();
    void parseExpr();
    void updateVars();
    void updateConsts();
    void recoverConsts();
    bool tryWatch();
    
    void mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event );
    

public slots:   
    void setWatch(QString newWatch);
    void setExpr(QString newExpr);
    void getNextLink(Link * link);

private:
    int id;
    static int counter;
    QString watch;
    QString Expr;
    
    int fail;
    
    QList<WatchItem *> watchList;
    QList<BoundVarItem *> varList;
    QList<Link *> inConstList;
    QList<Link *> outConstList;
    
    TransEditDialog * transDialog;

};


/*!
 * @brief Třída pro vnitřní reprezentaci watch výrazu
 */
class WatchItem {
    
public: 
    WatchItem(QString newArg1, QString newArg2, QString newOp);

    QString getFirstArg();
    QString getSecondArg();
    myOps getWatchOp();
    
    void setFirstArg(QString newArg);
    void setSecondArg(QString newArg);
    void setWatchOp(myOps newOp);
    
private:
    QString firstArg;
    QString secondArg;
    myOps watchOp;
};

/*!
 * @brief Třída pro vnitřní reprezentaci proměnných vázaných ve výrazech
 */
class BoundVarItem {
    
public: 
    BoundVarItem(QString newVar, int newVal);

    QString getVar();
    int getVal();
    
    void setVar(QString newVar);
    void setVal(int newVal);
    
private:
    QString var;
    int val;
};

/*!
 * @brief Dialog pro úpravu výrazů na přechodu
 */
class TransEditDialog : public QDialog {
    Q_OBJECT

public:
    TransEditDialog(Trans * newTrans, QWidget *parent = 0);
    
    void editDialog();
    
private slots:
    void acceptSlot();
    void editSlot();
    
signals:
    void setWatch(QString); /*!< Signál pro nastavení watch */
    void setExpr(QString); /*!< Signál pro nastavení výrazu */
    void addItem(Trans *); /*!< Signál pro přidání přechodu do scény */
    
private:
    QLabel *label;
    QLineEdit *watchLineEdit;
    QLineEdit *ExprLineEdit;
    QPushButton *acceptButton;
    QPushButton *closeButton;
    
    Trans * trans;
};

#endif

