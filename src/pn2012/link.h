/**
 *  @file  link.h
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 *
 *  @note Žádný kód z tohoto modulu nebyl vygenerován automaticky.
 *          
 *  Obsahuje definice tříd pro praci s hranami.
 * 
 */

#ifndef LINK_H
#define LINK_H

#include <QGraphicsLineItem>
#include <QDialog>

class QLineEdit;
class QLabel;
class QPushButton;

class PnItem;
class LinkItem;


/*! \brief Link: Třída pro reprezentaci hran.
 *  Dědí chování tříd QGraphicsLineItem a QObject.
 */
class Link : public QObject, public QGraphicsLineItem
{
    Q_OBJECT
public:
    /*! Konstruktor třídy. Nastaví vstupní a vystupní objekty hrany.
     * Vloží také informaci o hraně do spojených objektů.
     * Nakonec nastaví důležité flagy, Z-hodnotu objektu, barvu a zavola metodu trackNodes().
        \param fromPnItem vstupní objekt
        \param toPnItem výstupní objekt
        \sa trackNodes()
    */
    Link(PnItem *fromPnItem, PnItem *toPnItem);
    /*! Destruktor třídy. Při smazání se odebere ze seznamu spojených objektů, pokud ještě existují.
    */
    ~Link();
    
    /*! Metoda pro přístup ke vstupnímu objektu hrany.
        \return Vrací vstupní objekt.
    */
    PnItem *fromPnItem() const;
    /*! Metoda pro přístup ke výstupnímu objektu hrany.
        \return Vrací výstupní objekt.
    */
    PnItem *toPnItem() const;
    /*! Metoda pro přístup k výrazu hrany.
        \return Vrací řetězec, který uchovává výraz.
    */
    QString getLambda() const;
    /*! Metoda pro přístup k seznamu výrazů na hraně.
        \return Vrací seznam referencí na objekty s výrazy.
        \sa LinkItem
    */
    QList<LinkItem *> getItemList();
    /*! Metoda pro získání informace o další vhodné hodnotě tokenu z místa.
     *  Vysílá signál overflow() pokud jsou všechny hodnoty prošlé.
        \return Hodnota dalšího vhodného tokenu.
        \sa overflow()
    */
    int getNextVal();
    /*! Metoda pro získání názvu proměnné/konstanty na hraně.
        \return QString Název proměnné/konstanty na hraně.
        \sa LinkItem::getVarName()
    */
    QString getVar();
    /*! Metoda pro odebrání určitého počtu tokenů z místa.
        \param tokenVal Hodnota tokenu k odebrání.
        \sa LinkItem::getTokenNum()
    */
    void removeToken(int tokenVal);
    /*! Metoda pro přidání určitého počtu tokenů do místa.
        \param tokenVal Hodnota tokenu k přidání.
        \sa LinkItem::getTokenNum()
    */
    void addToken(int tokenVal);
    
    /*! Metoda pro nastavení barvy objektu pro vykreslení.
     * \param color Nová barva objektu.
    */
    void setColor(const QColor &color);
    /*! Metoda, která vytvoří vlastní spoj mezi objekty.
    */
    void trackNodes();
    /*! Redefinice metody pro vykreslení hrany.
     *  Vykresluje hranu v klientovi, spolecne s výrazem a kolečkem ve směru hrany.
     *  \param painter Reference objektu pro vykreslení.
     *  \param option Reference objektu pro získání informací o objektu.
    */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    /*! Metoda pro nastavení vstupního objektu
     *  \param item Vstupní objekt. 
    */
    void setFromPnItem(PnItem * item);
    /*! Metoda pro nastavení výstupního objektu
     *  \param item Výstupní objekt. 
    */
    void setToPnItem(PnItem * item);
    /*! Metoda pro vytvoření ItemList objektů z řetězce výrazu hrany.
     *  Resetuje také iterátor u míst.
     *  \sa Place::resetIter()
    */
    void update();
    
    
    bool checkConst();
public slots:   
    /*! Slot pro nastavení řetězce výrazu hrany.
     * \param newLambda Nový řetězec výrazu.
    */
    void setLambda(QString newLambda);
    
signals:
    /*! Signál pro získání nové použitelné hodnoty vedlejší hrany při vyzkoušení všech hodnot této hrany.
    *   \param link Reference této hrany.
    */
    void overflow(Link * link);

private:
    static int counter;
    QString linkLambda;

    PnItem *myFromPnItem;
    PnItem *myToPnItem;
    
    QList<LinkItem *> itemList;
};

/*! \brief LinkItem: Třída pro reprezentaci výrazu na hraně.
 *  Třída obsahuje informaci o názvu proměnné/hodnotě konstany a počtu tokenů pro přidání a odebrání z míst.
 */
class LinkItem {
    
public: 
    /*! Konstruktor třídy. Nastaví jméno proměnné/konstanty  a počet.
        \param name jméno proměnné/konstanty
        \param num počet
        \sa trackNodes()
    */
    LinkItem(QString name, QString num);
    /*! Metoda pro nastavení jména proměnné/konstanty.
     * \param newName Nové jméno proměnné/konstanty.
    */
    void setVarName(QString newName);
    /*! Metoda pro nastavení počtu tokenů pro přidání a odebrání z míst.
     * \param newNum Nový počet.
    */
    void setTokenNum(QString newNum);
    /*! Metoda pro získání jména proměnné/konstanty.
     * \return Jméno proměnné/konstanty.
    */
    QString getVarName();
    /*! Metoda pro získání počtu tokenů pro přidání a odebrání z míst.
     * \return Počet tokenů.
    */
    QString getTokenNum();
    
private:
    QString varName;
    QString tokenNum;
};

/*! \brief LineEditDialog: Třída dialogu pro přidání a editaci hrany.
 *  Dědí chování třídy QDialog.
 */
class LineEditDialog : public QDialog {
    Q_OBJECT

public:
    /*! Konstruktor třídy. Vytvoří potřebná pole a tlačítka a vhodně je rozloží.
        \param parent Rodič dialogu.
    */
    LineEditDialog(QWidget *parent);
    
public slots:
    /*! Slot, který se vykoná po potvrzení dialogu.
     *  Vyšle signály setLambda() a close().
    */
    void acceptSlot();
    
signals:
    /*! Signál pro nastavení výrazu u hrany.
     *  \param Nový řetězec výrazu.
    */
    void setLambda(QString);
    
private:
    QLabel *label;
    QLineEdit *lambdaLineEdit;
    QPushButton *acceptButton;
    QPushButton *closeButton;
};

#endif

