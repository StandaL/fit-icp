/**
 *  @file pnitem.h
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Žádný kód z tohoto modulu nebyl vygenerován automaticky.
 * 
 *  Obsahuje společné definice pro práci s místy a přechody.
 *
 */

#ifndef PNITEM_H
#define PNITEM_H

#include <QApplication>
#include <QGraphicsItem>
#include <QList>

#include "mainwindow.h"

class Link;

/*! \brief PnItem: Třída pro definici společného chování objektů petriho sítí (míst a přechodů).
 *	Dědí chování třídy QGraphicsObject.
 */
class PnItem : public QGraphicsObject
{
    Q_OBJECT
public:
	/*! Konstruktor třídy. Nastaví flagy pro povolení posunu, selekce a zasílání zpráv o změně pozice.
	*/
    PnItem();
    /*! Destruktor třídy. Zajistí nastavení referencí vstupních a výstupních hran na NULL.
     * \sa Link::setToPnItem()
     * \sa Link::setFromPnItem()
	*/
    ~PnItem();
    /*! Metoda pro přidání hrany do seznamu vstupních hran.
		\par link Reference vstupní hrany.
	*/
    void addInLink(Link *link);
    /*! Metoda pro přidání hrany do seznamu výstupních hran.
		\par link Reference výstupní hrany.
	*/
    void addOutLink(Link *link);
    /*! Metoda pro odebrání hrany ze seznamu vstupních hran.
		\par link Reference vstupní hrany.
	*/
    void removeInLink(Link *link);
    /*! Metoda pro odebrání hrany ze seznamu výstupních hran.
		\par link Reference výstupní hrany.
	*/
    void removeOutLink(Link *link);

protected:
    /*! Metoda pro odchycení změny objektu.
     * 	Při změně pozice upraví hrany jdoucí z/do objektu.
		\par change Proměnná udávající typ změny.
		\par value
		\sa Link::trackNodes()
	*/
    QVariant itemChange(GraphicsItemChange change,
                            const QVariant &value);
    QList<Link *> myInLinks;
    QList<Link *> myOutLinks;
    friend class mainWindow;
    friend class pThread;

};

/*! \brief PnItem: Třída reprezentující token místa.
 */
class PnToken {
public:
    /*! Konstruktor třídy. Nastaví název proměnné/konstanty  a počet na 0.
	*/
    PnToken();
    /*! Přetížený konstruktor třídy. Nastaví název proměnné/konstanty a jejich počet dle parametrů.
     * 	\par id Název proměnné/konstanty.
     * 	\par num Počet tokenů.
	*/
    PnToken(int id, int num);
    /*! Metoda získání názvu proměnné/konstanty.
		\return Vrací název proměnné/konstanty.
	*/
    int getTokenID() const;
    /*! Metoda získání počtu tokenů.
		\return Vrací počet tokenů.
	*/
    int getTokenNum() const;
    /*! Metoda nastavení názvu proměnné/konstanty.
		\par newID Nový název proměnné/konstanty.
	*/
    void setTokenID(int newID);
    /*! Metoda nastavení názvu počtu tokenů.
		\par newNum Nový počet tokenů.
	*/
    void setTokenNum(int newNum);
    /*! Metoda pro přetížení operátoru == pro jednoduché porování dle názvu proměnné/konstanty.
		\par other Objekt, se kterým porovnáváme tento objekt.
	*/
    bool operator== (const PnToken & other ) const;
    
private:
    int tokenID;
    int tokenNum;
};

#endif
