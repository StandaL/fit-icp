/**
    Autor:  Petr Kubat, xkubat11@stud.fit.vutbr.cz
    Modul:  scene.h
    Popis:  Obsahuje definice trid pro praci se scenou.
    
    Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
*/


#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMouseEvent>
#include <iostream>

/*!
 * Výčtový typ pro akci nad zobrazením
 */
enum myItems {SMOVE, SPLACE, SLINK, STRANS};

/*!
 * @brief Třída zobrazení části grafického plátna
 * 
 * Třída implementující zobrazování vybrané části grafického plátna a některých
 * akcí nad tímto zobrazením
 */
class myView : public QGraphicsView
{
    Q_OBJECT

signals:
    void createItem(myItems sel, QPoint point);

public:
    myView();
    myView(QGraphicsScene * scene);
    void mouseReleaseEvent ( QMouseEvent * event );
    void mousePressEvent ( QMouseEvent * event );
    void keyReleaseEvent ( QKeyEvent * keyEvent );
    
public slots:
    void setPlaceItem();
    void setMoveItem();
    void setLinkItem();
    void setTransItem();
    
signals:
    void deleteItems();

private:
    myItems selItem;
};

#endif
