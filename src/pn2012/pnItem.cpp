/**
 *  @file pnItem.cpp 
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Žádný kód z tohoto modulu nebyl vygenerován automaticky.
 * 
 *  Obsahuje chování, které je společné pro místa a přechody.
 * 
 */

#include <QtGui>

#include "pnItem.h"
#include "link.h"

// konstruktor
PnItem::PnItem() {
    // dulezite flagy - pohyb, selekce a zjisteni zmeny polohy
    setFlags(ItemIsMovable | ItemIsSelectable | ItemSendsGeometryChanges);
}

PnItem::~PnItem() {
    foreach(Link *link, myInLinks)
        link->setToPnItem(NULL);
    foreach(Link *link, myOutLinks)
        link->setFromPnItem(NULL);
}

// redefinice funkce pro osetreni zmeny objektu
QVariant PnItem::itemChange(GraphicsItemChange change,
                          const QVariant &value)
{
    // pokud doslo zmene, zmenime i linky
    if (change == ItemPositionHasChanged) {
        foreach (Link *link, myInLinks)
            link->trackNodes();
    }
    if (change == ItemPositionHasChanged) {
        foreach (Link *link, myOutLinks)
            link->trackNodes();
    }
    // event propagujeme dal
    return QGraphicsItem::itemChange(change, value);
}

// funcke pro pridani a odebrani linku z vectoru
void PnItem::addInLink(Link *link)
{
    myInLinks.append(link);
}

void PnItem::addOutLink(Link *link)
{
    myOutLinks.append(link);
}

void PnItem::removeInLink(Link *link)
{
    int i = myInLinks.indexOf(link);
    myInLinks.removeAt(i);
}

void PnItem::removeOutLink(Link *link)
{
    int i = myOutLinks.indexOf(link);
    myOutLinks.removeAt(i);
}

// PnToken
PnToken::PnToken() {
    tokenID = 0;
    tokenNum = 0;
}

PnToken::PnToken(int id, int num) {
    tokenID = id;
    tokenNum = num;
}
    
int PnToken::getTokenID() const {
    return tokenID;
}

int PnToken::getTokenNum() const {
    return tokenNum;
}

void PnToken::setTokenID(int newID) {
    tokenID = newID;
}
void PnToken::setTokenNum(int newNum) {
    tokenNum = newNum;
}
// metoda pro porovnavani Tokenu, pouziva ji QList v metode contains()
bool PnToken::operator== (const PnToken & other ) const {
    if (this->tokenID == other.tokenID)
        return true;
    else return false;
}
