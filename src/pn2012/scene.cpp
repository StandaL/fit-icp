/**
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 *  @file scene.cpp
 *
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Modul pro praci se scenou. Obsahuje redefinice nekterych eventu a
 *  definice nekolika metod.
 * 
 */

#include <QtGui>
#include <iostream>

#include "scene.h"
#include "trans.h"

class Trans;

// konstruktory
myView::myView() : QGraphicsView()
{}

myView::myView(QGraphicsScene * scene) : QGraphicsView(scene)
{}

void myView::setPlaceItem() {
    /**
     * Třída ukládající informaci o tom, že je stisknuté tlačítko pro vložení místa
     */
    this->selItem = SPLACE;
}

void myView::setMoveItem() {
    /**
     * Třída ukládající informaci o tom, že je stisknuté tlačítko pohyb objektem
     */
    this->selItem = SMOVE;
}

void myView::setLinkItem() {
    /**
     * Třída ukládající informaci o tom, že je stisknuté tlačítko pro vložení spojení
     */    
    this->selItem = SLINK;
}

void myView::setTransItem() {
    /**
     * Třída ukládající informaci o tom, že je stisknuté tlačítko pro vložení přechodu
     */    
    this->selItem = STRANS;
}

void myView::mouseReleaseEvent ( QMouseEvent * event )
    {
        /**
         * Funkce ošetřující kliknutí do zobrazení
         */
        // pokud jde o leve tlacitko, tak odesleme signal s pozici kurzoru a stavem
        if(event->button() == Qt::LeftButton)
        {
            emit createItem(this->selItem, event->pos()); 
        }
        // prozatim sikovne vytvoreny blok pro testovaci ucely pravym tlacitkem
        if(event->button() == Qt::RightButton) {


        }

        // kliknuti propagujeme dal
        QGraphicsView::mouseReleaseEvent(event); 
    }; 

// funkce, zajistujici reakci na left click jen v pripade stavu SMOVE
void myView::mousePressEvent ( QMouseEvent * event )
    {
        /**
         * Funkce zajišťující reakci na levé tlačítko myši je-li stisknuto tlačítko
         * pro pohyb objekty
         */
      // pokud jde o leve tlacitko a mame zaskrtle tlacito pro pohyb objekty, tak kliknuti propagujeme dal
    if((event->button() == Qt::LeftButton) && (selItem == SMOVE))
        QGraphicsView::mousePressEvent(event);
    // jinak event ignorujeme
    };
    
void myView::keyReleaseEvent ( QKeyEvent * keyEvent ) {
    /**
     * Funkce, díky níž lze mazat objekty stiskem klávesy Delete
     */
    if (keyEvent->key() == Qt::Key_Delete) {
        emit deleteItems();
    }
}
