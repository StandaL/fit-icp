/**
 *  @file xmlhandler.h
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Obsahuje vesmes jen includy potrebnych hlavickovych souboru.
 *
 */

#ifndef _XMLHANDLER_CPP
#define _XMLHANDLER_CPP

#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include <QtGui>
#include <QGraphicsScene>
#include <QFile>
#include <QIODevice>
#include <QString>
#include <QDebug>

#include <iostream>
#include <fstream>

#include "mainwindow.h"
#include "trans.h"
#include "link.h"
#include "place.h"


#endif
