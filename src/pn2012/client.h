/**
 *  @file client.h
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 *  
 *  Hlavičkový soubor klienta, definuje třídu okna dotazu o
 *  informacích o serveru
 * 
 */
 
#ifndef _CLIENT_H
#define _CLIENT_H

#include <QtGui>
#include <QApplication>
#include <QGraphicsItem>
#include <QDialog>

/*! @brief Třída okna dotazu o serveru */
class serverDialog : public QDialog {
    Q_OBJECT

public:
    serverDialog(QWidget *parent);
    
public slots:
    void acceptSlot();
    
signals:
    void setServer(QString);
    void setPort(int);
    void getSimData();
    
private:
    QLabel *label;  /*!< Text před textovými poli */
    QLineEdit *servLineEdit;    /*!< Textové pole názvu serveru */
    QLineEdit *portLineEdit;    /*!< Textové pole portu */
    QPushButton *acceptButton;  /*!< Tlačítko OK */
    QPushButton *closeButton;   /*!< Tlačítko Cancel */
    
    friend class mainWindow;
};

/*! @brief Třída okna dotazu na přihlašovací údaje */
class authDialog : public QDialog {
    Q_OBJECT

public:
    authDialog(QWidget *parent);
    
public slots:
    void acceptSlot();
    
signals:
    void setLogin(QString);
    void setPasswd(QString);
    void sendAuth();
    
private:
    QLabel *label;  /*!< Text před textovými poli */
    QLineEdit *loginLineEdit;    /*!< Textové pole názvu serveru */
    QLineEdit *passwdLineEdit;    /*!< Textové pole portu */
    QPushButton *acceptButton;  /*!< Tlačítko OK */
    QPushButton *closeButton;   /*!< Tlačítko Cancel */
    
    friend class mainWindow;
};

#endif
