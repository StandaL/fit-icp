/**
 *  @file place.h
 *  @author  Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Obsahuje definice trid pro praci s hranami.
 *   
 */

#ifndef PLACE_H
#define PLACE_H

#include <QApplication>
#include <QGraphicsItem>
#include <QList>
#include <QDialog>

#include "pnItem.h"

class QLineEdit;
class QLabel;
class QPushButton;

class PlaceEditDialog;

/**
 * @brief Třída míst
 * 
 * Třída implementující chování míst, zahrnuje definici tokenů, vykreslení
 * grafického objektu místa a práce s ním.
 */
class Place : public PnItem
{
    Q_OBJECT
    
public:
    Place();
    Place(QWidget *parent);
    ~Place();
    
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    QRectF outlineRect() const;
    
    QString getTokenString();
    void updateString();
    void updateTokens();
    
    void showDialog();
    void showEditDialog();
    
    int getToken(int count);
    int getToken(int value, int count);
    int getListSize();
    void resetIter();
    void removeToken(int tokenVal);
    void addToken(int tokenVal);
    
    void mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event );
    
    
public slots:
    void setTokenString(QString newString);
    
private:
    int currIter;
    
    QString tokenString;
    QList<PnToken *> tokenList;
    
    PlaceEditDialog * placeDialog;


};

/*!
 * @brief Třída pro dialog úpravy místa
 */
class PlaceEditDialog : public QDialog {
    Q_OBJECT

public:
    PlaceEditDialog(Place * newPlace, QWidget *parent = 0);
    
    void editDialog();
    
public slots:
    void acceptSlot();
    void editSlot();
    
signals:
    void addItem(Place *);
    
private:
    QLabel *label;
    QLineEdit *tokenLineEdit;
    QPushButton *acceptButton;
    QPushButton *closeButton;
    
    Place * place;
};

#endif
