/**
 *  @file  mainwindow.cpp
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 *  @author Stanislav Laznicka, xlazni08@stud.fit.vutbr.cz
 *
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 *          
 *  Modul popisujici chovani grafickeho prostredi aplikace.
 *  V tomto okne take probiha veskera manipulace s objekty site.
 * 
 */

#include <QtGui>
#include <iostream>

#include "mainwindow.h"
#include "scene.h"
#include "place.h"
#include "trans.h"
#include "link.h"

class Place;
class Trans;
class myView;
class Link;
class TransEditDialog;

//Konstruktor
mainWindow::mainWindow()
{
    /**
     * Konstruktor GUI, vytvoří okno s hlavní nabídkou a statusbarem,
     * grafické plátno, zobrazí část tohoto plátna, přiřadí akce k tlačítkům a 
     * tyto tlačítka zobrazí
     * 
     * @todo Otevření více pracovních ploch najednou
     */
    //Vytvorime scenu
    scene = new QGraphicsScene(0,0,2000,2000);
    //a pohled na ni
    view = new myView(scene);
    // nastavime ruzne vlastnosti
    view->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    view->setDragMode(QGraphicsView::RubberBandDrag);
    view->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    view->setContextMenuPolicy(Qt::ActionsContextMenu);
    // pouzijeme pohled jako centralni widget
    setCentralWidget(view);
    
    // propojime signal se slotem pro vytvoreni objektu
    connect(view, SIGNAL(createItem(myItems, QPoint)), this, SLOT(createItem(myItems, QPoint)));
    connect(view, SIGNAL(deleteItems()), this, SLOT(deleteItems()));
    
    // vytvorime status bar, akce, menu a toolbary
    createStatusBar();
    createActions();
    createMenu();
    createToolBars();

    // nastavime titulek okna
    setWindowTitle(tr("Petri net"));
    
    // a nastavime oznaceny objekt na neexistujici
    marked = -1;
}

void mainWindow::createItem(myItems sel, QPoint point) {
    /**
     * @param sel Typ vytvářeného objektu (SPLACE, STRANS, SLINK)
     * @param point Pozice, na které má být vytvářený objekt
     * 
     * Slot pro tvorbu míst, přechodů a spojení mezi nimi
     */
    // switch podle toho, jaky item tvorime
    switch(sel) {
        case SPLACE: {
            // vytvorime nove misto na pozici kurzoru
            Place *newPlace = new Place(this);
            // posuneme ho na spravne misto
            newPlace->setPos(view->mapToScene(point));
            newPlace->showDialog();
            break;
        }
        case STRANS: {
            // vytvorime nove misto na pozici kurzoru
            Trans *newTrans = new Trans(this);
            // posuneme ho na spravne misto
            newTrans->setPos(view->mapToScene(point));
            newTrans->showDialog();
            break;
        }
        case SLINK: {
            // vytvorime hranu propojujici dva objekty
            
            // pomocna promenna pPlace pro uchovani objektu z predchoziho volani
            static PnItem *pPnItem = NULL;
            // jde-li o prvni volani a na pozici kurzoru je objekt
            if (!pPnItem && view->itemAt(point))
                // pokud ano, ulozime si odkaz na prvni objekt do pomocne promenne
                pPnItem = dynamic_cast<PnItem*>(view->itemAt(point));
            else if (view->itemAt(point)) {
                // ulozime si odkaz na objekt do pomocne promenne
                PnItem *pTmpPnItem = dynamic_cast<PnItem*>(view->itemAt(point));
                // pokud jde o pnItem a zaroven se nejde o spojeni dvou stejnych objektu
                if (pTmpPnItem && (pTmpPnItem != pPnItem)) {
                    // zkontrolujeme, jestli jde o spoj misto -- prechod, pripadne prechod -- misto
                    if ((dynamic_cast<Trans*>(pPnItem) && dynamic_cast<Place*>(pTmpPnItem)) || (dynamic_cast<Place*>(pPnItem) && dynamic_cast<Trans*>(pTmpPnItem))) {
                        // jde-li o druhe volani, vytvorime novou hranu, ktera propouje objekty
                        Link *link = new Link(pPnItem, static_cast<PnItem*>(view->itemAt(point)));
                        LineEditDialog *testDialog = new LineEditDialog(this);
                        testDialog->show();
                        testDialog->raise();
                        testDialog->activateWindow();
                        connect(testDialog, SIGNAL(setLambda(QString)), link, SLOT(setLambda(QString)));
                        // vlozime hranu do sceny
                        scene->addItem(link);
                    }
                }
                // a vynulujeme ukazatel
                pPnItem = NULL;
                
            }
            break;
        }
        default: {
            scene->update();
            break;
           }
    }
}

void mainWindow::addTrans(Trans * newTrans) {
    /**
     * @param newTrans  Ukazatel na přidávaný přechod
     * 
     * Přidá přechod do scény, navíc též ukazatel na něj uloží do QListu (kvůli
     * generování xml)
     */
    scene->addItem(newTrans);
    myTransList.append(newTrans);
}

void mainWindow::addPlace(Place * newPlace) {
    /**
     * @param newPlace  Ukazatel na přidávané místo
     * 
     * Přidá místo do scény, ukazatel na něj uloží do QListu (kvůli generování
     * xml)
     */
    newPlace->updateTokens();
    newPlace->updateString();
    scene->addItem(newPlace);
    myPlacesList.append(newPlace);
}

void mainWindow::addLink(Link * newLink) {
    /** 
     * @param newLink  Ukazatel na přidávané spojení
     *  
     * Přidá do scény spojení 
     */
    scene->addItem(newLink);
}

void mainWindow::deleteItems() {
    /**
     * Tato funkce smaže veškeré označené objekty (na stisk klávesy Delete)
     */
    QList<QGraphicsItem *> items = scene->selectedItems();
    foreach (QGraphicsItem * item, items) {
        Place * place = dynamic_cast<Place*>(item);
        if (place)
            myPlacesList.removeAll(place);
        Trans * trans = dynamic_cast<Trans*>(item);
        if (trans)
            myTransList.removeAll(trans);
        scene->removeItem(item);
    }
    qDeleteAll(items);
}

void mainWindow::newScene() {
    /**
     * Promaže současnou scénu a vytvoří nové, čisté plátno
     */
    // Nejprve promazat starou scenu
    qDeleteAll(myTransList);
    myTransList.clear();
    qDeleteAll(myPlacesList);
    myPlacesList.clear();
    scene->clear();
    //Vytvorime scenu
    scene = new QGraphicsScene(0,0,2000,2000);
    //a pohled na ni
    view = new myView(scene);
    // nastavime ruzne vlastnosti
    view->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    view->setDragMode(QGraphicsView::RubberBandDrag);
    view->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    view->setContextMenuPolicy(Qt::ActionsContextMenu);
    // pouzijeme pohled jako centralni widget
    setCentralWidget(view);
    
    // propojime signal se slotem pro vytvoreni objektu
    connect(view, SIGNAL(createItem(myItems, QPoint)), this, SLOT(createItem(myItems, QPoint)));
    connect(view, SIGNAL(deleteItems()), this, SLOT(deleteItems()));
    
    // znovu propojime signaly akci
    connect(itemPlaceAction, SIGNAL(triggered()), view, SLOT(setPlaceItem()));
    connect(itemMoveAction, SIGNAL(triggered()), view, SLOT(setMoveItem()));
    connect(itemLinkAction, SIGNAL(triggered()), view, SLOT(setLinkItem()));
    connect(itemTransAction, SIGNAL(triggered()), view, SLOT(setTransItem()));
    
    filename = "";
    
}

void mainWindow::createActions()
{
    /**
     * Vytvoří akce okna, propojí je se sloty, které určují, co tyto akce budou
     * provádět
     */
    // menu
    newAction = new QAction(tr("&Novy"), this);
    newAction->setShortcut(QKeySequence::New);
    newAction->setStatusTip(tr("Vytvorit novy soubor."));
    connect(newAction, SIGNAL(triggered()), this, SLOT(newScene()));
    openAction = new QAction(tr("&Otevrit"), this);
    openAction->setShortcut(QKeySequence::Open);
    openAction->setStatusTip(tr("Otevrit soubor."));
    connect(openAction, SIGNAL(triggered()), this, SLOT(openXml()));
    saveAction = new QAction(tr("&Ulozit"), this);
    saveAction->setShortcut(QKeySequence::Save);
    connect(saveAction, SIGNAL(triggered()), this, SLOT(saveXml()));
    saveAction->setStatusTip(tr("Ulozit soubor."));
    saveAsAction = new QAction(tr("Ulozit &jako"), this);
    saveAsAction->setShortcut(QKeySequence::SaveAs);
    connect(saveAsAction, SIGNAL(triggered()), this, SLOT(saveAsXml()));
    saveAsAction->setStatusTip(tr("Ulozit soubor jako."));
    exitAction = new QAction(tr("U&koncit"), this);
    exitAction->setShortcut(tr("Ctrl+Q"));
    exitAction->setStatusTip(tr("Ukoncit program."));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
    reloadAction = new QAction(tr("&Reload"), this);
    reloadAction->setStatusTip(tr("Znovu nahrat soubor."));
    connect(reloadAction, SIGNAL(triggered()), this, SLOT(xmlload()));
    
    // levy toolbar
    // button pro misto
    itemPlaceAction = new QAction(tr("Misto"), this);
    itemPlaceAction->setStatusTip(tr("Vytvorit nove misto."));
    // propojime se slotem
    connect(itemPlaceAction, SIGNAL(triggered()), view, SLOT(setPlaceItem()));
    // checkable = true, potrebujeme ty tlacitka "zaskrtavaci"
    itemPlaceAction->setCheckable(true);
    // move button
    itemMoveAction = new QAction(tr("Move"), this);
    itemMoveAction->setStatusTip(tr("Posunout objekt."));
    // propojime se slotem
    connect(itemMoveAction, SIGNAL(triggered()), view, SLOT(setMoveItem()));
    itemMoveAction->setCheckable(true);
    // link button
    itemLinkAction = new QAction(tr("Link"), this);
    itemLinkAction->setStatusTip(tr("Vytvorit spojeni."));
    // propojime se slotem
    connect(itemLinkAction, SIGNAL(triggered()), view, SLOT(setLinkItem()));
    itemLinkAction->setCheckable(true);
    // transition button
    itemTransAction = new QAction(tr("Trans"), this);
    itemTransAction->setStatusTip(tr("Vytvorit novy prechod."));
    // propojime se slotem
    connect(itemTransAction, SIGNAL(triggered()), view, SLOT(setTransItem()));
    itemTransAction->setCheckable(true);
    
    // button pro spusteni simulace
    fullSimAction = new QAction(tr("Sim"), this);
    fullSimAction->setStatusTip(tr("Spustit simulaci."));
    // propojime se slotem
    connect(fullSimAction, SIGNAL(triggered()), this, SLOT(simTillEnd()));

    // button pro označení objektu pro simulaci a krokování
    markAction = new QAction(tr("Step"), this);
    markAction->setStatusTip(tr("Oznacte objekt pro krok simulace."));
    connect(markAction, SIGNAL(triggered()), this, SLOT(step()));

    // button pro odpojení ze serveru
    disconnectAction = new QAction(tr("Odpojit se"), this);
    disconnectAction->setStatusTip(tr("Klikněte pro odpojení ze serveru."));
    connect(disconnectAction, SIGNAL(triggered()), this, SLOT(disconnectFromServer()));
    
    // itemgroup, spoji tlacitka do skupiny - zaskrtnute vzdy jen jedno
    itemGroup = new QActionGroup(this);
    itemGroup->addAction(itemPlaceAction);
    itemGroup->addAction(itemMoveAction);
    itemGroup->addAction(itemLinkAction);
    itemGroup->addAction(itemTransAction);
}

void mainWindow::simTillEnd() {
    /** 
     * Zašle serveru informaci, že má provést kompletní simulaci, zachovává
     * označený objekt
     */
    int oldmarked = marked;
    marked = -1;
    connectToServer();
    marked = oldmarked;
}

void mainWindow::step() {
    /**
     * Zašle serveru informaci, že má provést 1 krok na právě označeném přechodu
     */
    QList<QGraphicsItem *> selected = scene->selectedItems();
    if(selected.size() == 1) {   // mam te
        Trans *trans = dynamic_cast<Trans *>(selected[0]);
        if(trans != NULL) {  // je to vskutku prechod
            marked = myTransList.indexOf(trans);
        }
    }
    else marked = -1;
    if(marked > -1) connectToServer();
}

void mainWindow::createMenu()
{
    /**
     * Vytvoří hlavní nabídku
     */
    fileMenu = menuBar()->addMenu(tr("&Soubor"));
    fileMenu->addAction(newAction);
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
    fileMenu->addAction(saveAsAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);
}

void mainWindow::createStatusBar()
{
    /**
     * Přidá do okna statusbar
     */
    statusBar();
}

// vytvoreni toolbaru
void mainWindow::createToolBars()
{
    /**
     * Vytvoří vrchní panel nástrojů a také panel nástrojů vlevo
     */
    // menu toolbar, defaultne horizontalni nahore
    fileToolBar = addToolBar(tr("&Soubor"));
    fileToolBar->addAction(newAction);
    fileToolBar->addAction(openAction);
    fileToolBar->addAction(saveAction);
    fileToolBar->addAction(reloadAction);
    fileToolBar->addAction(fullSimAction);
    fileToolBar->addAction(markAction);
    fileToolBar->addAction(disconnectAction);
    
    // item toolbar
    itemToolBar = addToolBar(tr("&Nastroje"));
    // hodime ho verikalne vlevo
    addToolBar(Qt::LeftToolBarArea, itemToolBar);
    itemToolBar->addActions(itemGroup->actions());
    
}
