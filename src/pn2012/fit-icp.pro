######################################################################
# Automatically generated by qmake (2.01a) ?t 5 3 08:37:22 2012
######################################################################

TEMPLATE = app
TARGET = pn2012 
DEPENDPATH += .
INCLUDEPATH += .
QT += network

# Input
HEADERS += client.h \
           link.h \
           mainwindow.h \
           place.h \
           pnItem.h \
           scene.h \
           trans.h \
           xmlhandler.h
SOURCES += client.cpp \
           link.cpp \
           main.cpp \
           mainwindow.cpp \
           place.cpp \
           pnItem.cpp \
           scene.cpp \
           trans.cpp \
           xmlhandler.cpp
