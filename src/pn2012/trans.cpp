/**
 *  @file trans.cpp
 *  @author Petr Kubat, xkubat11@stud.fit.vutbr.cz
 * 
 *  @note Zadny kod z tohoto modulu nebyl vygenerovan automaticky.
 * 
 *  Modul popisujici chovani prechodu v siti. V tomto modulu take 
 *  probiha vlastni simulace pomoci metody simulate().
 *   
 */

#include <QtGui>
#include <QRegExp>

#include "trans.h"
#include "pnItem.h"
#include "link.h"

#include <iostream>

int Trans::counter = 0;

class Link;

// konstruktor
Trans::Trans(QWidget *parent) {
    /**
     * @param parent QWidged objekt, s kterým bude tento nový objekt propojen
     * 
     * Vytvoří objekt přechodu a zavolá dialog pro úpravu jeho výrazů
     */
    // dulezite flagy - pohyb, selekce a zjisteni zmeny polohy
    setFlags(ItemIsMovable | ItemIsSelectable | ItemSendsGeometryChanges);
    // přidělení id pro jednotlivé přechody - důležité pro parsing
    id = counter;
    counter++;
    fail = 0;
    
    transDialog = new TransEditDialog(this);
    connect(transDialog, SIGNAL(addItem(Trans *)), parent, SLOT(addTrans(Trans *)));
    
}

Trans::Trans() {
    /**
     * Přetížený konstruktor pro použití na místech, kde není GUI (server)
     */
    transDialog = NULL;
    fail = 0;
}

// destruktor
Trans::~Trans() {
    if (transDialog)
        delete transDialog;
}

void Trans::showDialog() {
    /**
     * Zobrazí dialog pro úpravu výrazů na přechodu
     */
    transDialog->show();
    transDialog->raise();
    transDialog->activateWindow();
}

void Trans::paint(QPainter *painter,
                 const QStyleOptionGraphicsItem *option,
                 QWidget *)
{
    /**
     * Redefinice metody pro vykreslení, vytvoří grafický obdélníkový objekt
     * rozdělený na dvě části reprezentující přechod
     */
    // Barva obrysu
    QPen pen(Qt::darkBlue);
    // Pokud je misto vybrane, vykreslime obrys carkovane
    if (option->state & QStyle::State_Selected)
        pen.setStyle(Qt::DotLine);
    // nastavime parametru
    painter->setPen(pen);
    painter->setBrush(Qt::white);
    // a vykreslime
    QRectF rect = outlineRect();
    painter->drawRect(rect); // <-- rozmery
    // vypiseme straz
    painter->drawText(rect, Qt::AlignTop | Qt::AlignHCenter, watch);
    QPointF cenPoint = rect.center();
    // vykreslime caru oddelujici straz
    painter->drawLine(cenPoint + QPointF(-rect.width()/2, 0), cenPoint + QPointF(rect.width()/2, 0));
    // vypiseme vyraz
    painter->drawText(rect, Qt::AlignBottom | Qt::AlignHCenter, Expr);
}

QRectF Trans::boundingRect() const
{
    /**
     * Redefinice metody pro velikost grafického objektu
     * 
     * @return Vrací upravený grafický objekt
     */
    const int Margin = 1;
    // objekt plus pixel
    return outlineRect().adjusted(-Margin, -Margin, +Margin, +Margin);
}

QRectF Trans::outlineRect() const
{
    /**
     * Metoda pro vykreslení dostatečného prostoru pro vnitřní výrazy
     * 
     * @return Vrací upravený grafický objekt
     */
    // padding
    const int Padding = 5;
    // zjistime metriky pouzivane kodu
    QFontMetricsF metrics = qApp->font();
    // a vytvorime obdelniky okolo vstupnich textu
    QRectF rect1 = metrics.boundingRect(watch);
    QRectF rect2 = metrics.boundingRect(Expr);
    // zjistime, ktery obdelnik je sirsi a ten pouzijeme jako zaklad
    // pote zvetsime puvodni obdelnik o 15 pixelu a presuneme ho do stredu
    if (rect1.width() < rect2.width()) {
        rect2.adjust(-Padding, -Padding, +Padding, 15+Padding);
        rect2.translate(-rect2.center());
        return rect2;
    }
    else {
        rect1.adjust(-Padding, -Padding, +Padding, 15+Padding);
        rect1.translate(-rect1.center());
        return rect1;
    }
}

void Trans::setWatch(QString newWatch) {
    /**
     * @param newWatch Nový řetězec pro watch
     * 
     * Nastaví watch
     */
    watch = newWatch;
}

void Trans::setExpr(QString newExpr) {
    /**
     * @param newExpr Nový řetězec výrazu
     * 
     * Nastaví výraz přechodu
     */
    Expr = newExpr;
}

// metody pro vraceni stringu
QString Trans::getWatch() {
    /**
     * Vrací aktuální watch na přechodu
     */
    return watch;
}

QString Trans::getExpr() {
    /**
     * Vrací aktuální výraz na přechodu
     */
    return Expr;
}

void Trans::getNextLink(Link * link) {
    /**
     * Metoda pro simulaci - nalezení tokenu z dalšího spojení
     */
    int index = myInLinks.indexOf(link);
    // pokud uz dalsi link neni, fail
    if ((index + 1) == myInLinks.size())
        fail = 1;
    else {
        // jinak vezmeme dalsi link a zjistime mozny token
        link = myInLinks[index+1];
        QString var = link->getVar();
        int val = link->getNextVal();
        if (val != -1) {
            if (!var.toInt()) {
                foreach (BoundVarItem * item, varList) {
                    if (item->getVar() == var) {
                        item->setVal(val);
                        break;
                    }
                }
            }
        }
        else fail = 1;
    }
    
}

bool Trans::tryWatch() {
    /**
     * Metoda pro simulaci - otestování pravdivosti podmínky přechodu
     * 
     * @return true pokud podmínka splněna, false pokud ne
     */
    foreach(WatchItem * watch, watchList) {
        int arg1 = 0;
        int arg2 = 0;
        int found = 0;
        bool result;
        myOps op = watch->getWatchOp();
        if (watch->getFirstArg().toInt()) {
            arg1 = watch->getFirstArg().toInt();
            found = 1;
        }
        else {
            foreach(BoundVarItem * item, varList) {
                if (item->getVar() == watch->getFirstArg()) {
                    arg1 = item->getVal();
                    found = 1;
                    break;
                }
            }
        }
        if (!found) {
            fail = 1;
            return false;
        }
        found = 0;
        if (watch->getSecondArg().toInt()) {
            arg2 = watch->getSecondArg().toInt();
            found = 1;
        }
        else {
            foreach(BoundVarItem * item, varList) {
                if (item->getVar() == watch->getSecondArg()) {
                    arg2 = item->getVal();
                    found = 1;
                    break;
                }
            }
        }
        if (!found) {
            fail = 1;
            return false;
        }
        switch(op) {
            case OLOW: {
                result = (arg1 < arg2);
                break;
            }
            case OLEQ: {
                result = (arg1 <= arg2);
                break;
            }
            case OGEQ: {
                result = (arg1 >= arg2);
                break;
            }
            case OGRT: {
                result = (arg1 > arg2);
                break;
            }
            case OEQ: {
                result = (arg1 == arg2);
                break;
            }
            case ONEQ: {
                result = (arg1 != arg2);
                break;
            }
            default: {
                result = false;
                break;
            }
        }   
        return result;
    }
    return true;
}


int Trans::simulate() {
    /**
     * Metoda, která provede odsimulování kroku na daném přechodu
     * 
     * @return 0 pokud se nenalezne žádná vhodná kombinace nebo podmínka na 
     *          přechodu je nepravdivá, 1 pokud vše proběhne v pořádku
     * 
     * @todo Funkce zatím pracuje pouze pro tokeny větší 0
     */
    if (myInLinks.empty() && myOutLinks.empty())
        return 0;
    // parsujeme watch
    parseWatch();
    // updatujeme linky
    foreach (Link *link, myInLinks)
        link->update();
    foreach (Link *link, myOutLinks)
        link->update();
    // updatujeme navazane promenne
    updateVars();
    // presuneme hrany s konstantama do vlastnich seznamu
    updateConsts();
    
    // zjistime, jestli jsou v mistech potrebne tokeny pro konstanti hrany
    foreach (Link * link, inConstList) {
        if(!link->checkConst()) {
            recoverConsts();
            return 0;
        }
    }
    
    int found = 1;
    // propojime signaly
    foreach (Link * link, myInLinks)
             connect(link, SIGNAL(overflow(Link *)), this, SLOT(getNextLink(Link *)));
    
    // navazeme prvni kombinaci promennych
    foreach (Link *link, myInLinks) {
        QString var = link->getVar();
        int val = link->getNextVal();
        if (val != -1) {
            if (!var.toInt()) {
                foreach (BoundVarItem * item, varList) {
                    if (item->getVar() == var) {
                        item->setVal(val);
                        break;
                    }
                }
            }
        }
        else fail = 1;
    }
    // pokud se neco nenavazalo, hledame dal
    foreach (BoundVarItem * item, varList) {
            if (item->getVal() < 1) {
                found = 0;
                break;
            }
    }
    // pokud neproslo strazi, hledame dal
    if (!tryWatch())
        found = 0;
    // pokud fail, koncime
    if (fail) {
        recoverConsts();
        return 0;
    }
    int lolcode = 0; // rychly fix cykleni
    // obdobne v cyklu
    while(!found && !myInLinks.isEmpty()) {
        
        found = 1;
        int change = 0;
        
        Link * link = myInLinks[0];
        QString var = link->getVar();
        int val = link->getNextVal();
        if (val != -1) {
            if (!var.toInt()) {
                foreach (BoundVarItem * item, varList) {
                    if ((item->getVar() == var) && item->getVal() != val) {
                        item->setVal(val);
                        change = 1;
                        break;
                    }
                }
            }
        }
        else fail = 1;
        
        foreach (BoundVarItem * item, varList) {
                if (item->getVal() < 1) {
                    found = 0;
                    break;
                }
        }
        if (!tryWatch())
            found = 0;
        // pokud nenalezneme, nebo nedoslo-li ke zmenene - fail, take fail pokud cyklime po ste
        if (fail || !change || (lolcode > 1000)) {
            recoverConsts();
            return 0;
        }
        lolcode++;
    }
    // odebereme tokeny ze vstupnich mist
    foreach (Link *link, myInLinks) {
        QString var = link->getVar();
        foreach (BoundVarItem * item, varList) {
            if (item->getVar() == var) {
                link->removeToken(item->getVal());
            }
        }
    }
    foreach (Link * link, inConstList) {
        link->removeToken(link->getVar().toInt());
    }
    // rozparsujeme vyraz a namapujeme novou promennou
    parseExpr();
    // pridame tokeny do vystupnich mist
    foreach (Link *link, myOutLinks) {
        QString var = link->getVar();
        foreach (BoundVarItem * item, varList) {
            if (item->getVal() == -1) break;
            if (item->getVar() == var) {
                link->addToken(item->getVal());
            }
        }
    }
    foreach (Link * link, outConstList) {
        link->addToken(link->getVar().toInt());
    }
    recoverConsts();
    return 1;
}


void Trans::parseWatch() {
    /**
     * Metoda pro simulaci - parsování watch
     */
    qDeleteAll(watchList);
    watchList.clear();
    
    QRegExp rx("(\\w+)\\s*(<|<=|>=|>|==|!=)\\s*(\\w+)\\s*(&?)");
    int pos = 0;
    
    while ((pos = rx.indexIn(watch, pos)) != -1) {
        WatchItem *tempItem = new WatchItem(rx.cap(1), rx.cap(3), rx.cap(2));
        watchList.append(tempItem);
        pos += rx.matchedLength();
    }
}


void Trans::parseExpr() {
    /**
     * Metoda pro simulaci - parsování vyhodnocovacího výrazu
     */
    QRegExp rx("(\\w+)\\s*=\\s*(\\w+)(.*)");
    int pos = 0;
    
    if ((pos = rx.indexIn(Expr, pos)) != -1) {
        int tempVal = 0;
        QString newVar = rx.cap(1);
        QString arg = rx.cap(2);
        pos += rx.matchedLength();
        if (arg.toInt())
            tempVal = arg.toInt();
        else
             foreach (BoundVarItem * item, varList) {
                if (item->getVar() == arg) {
                    tempVal = item->getVal();
                    break;
                }
            }
        if (!rx.cap(3).isEmpty()) {
            QString rest = rx.cap(3);
            pos = 0;
            rx.setPattern("\\s*(\\+|-)?\\s*(\\w+)(.*)");
            while ((pos = rx.indexIn(rest, pos)) != -1) {
                QString op = rx.cap(1);
                arg = rx.cap(2);
                if (arg.toInt())
                    if (op == "+")
                        tempVal += arg.toInt();
                    else
                        tempVal -= arg.toInt();
                else
                     foreach (BoundVarItem * item, varList) {
                        if (item->getVar() == arg) {
                            if (op == "+")
                                tempVal += item->getVal();
                            else
                                tempVal -= item->getVal();
                            break;
                        }
                    }
                rest = rx.cap(3);
                pos = 0;
            }
        }
        int found = 0;
        foreach (BoundVarItem * item, varList)
            if (item->getVar() == newVar) {
                item->setVal(tempVal);
                found = 1;
                break;
            }
        if (!found) {
            BoundVarItem * tempItem = new BoundVarItem(newVar, tempVal);
            varList.append(tempItem);
        }
    }
}

// metoda pro nacteni promennych z linku a expr
void Trans::updateVars() {
    /**
     * Metoda pro načtení proměnných ze spojení a z výrazů
     * 
     * @todo Výraz a kontrola, jestli uz objekt není v listu
     */
    
    // vymazeme seznam promennych
    qDeleteAll(varList);
    varList.clear();
    
    // projdeme vsechny linky
    foreach (Link * link, myInLinks) {
        // ziskame seznam vyrazu kazdeho linku
        QList<LinkItem *> itemList = link->getItemList();
        foreach (LinkItem * item, itemList) {
            if (!item->getVarName().toInt()) {
                // projdeme seznam vyrazu a naplnime objekt
                BoundVarItem * tempItem = new BoundVarItem(item->getVarName(), 0);
                // objekt vlozime do seznamu
                varList.append(tempItem);
            }
        }
    }
        
}

void Trans::updateConsts() {
    /**
     * Metoda pro přesun spojení pouze s konstantami do vlastních seznamů
     */
    foreach (Link * link, myInLinks) {
        if (link->getVar().toInt()) {
            myInLinks.removeOne(link);
            inConstList.append(link);
        }
    }
    foreach (Link * link, myOutLinks) {
        if (link->getVar().toInt()) {
            myOutLinks.removeOne(link);
            outConstList.append(link);
        }
    }
}

void Trans::recoverConsts() {
    /**
     * Metoda pro přesun linků pouze s konstantami zpět do in/ou seznamů
     */
    foreach (Link * link, inConstList) {
        myInLinks.append(link);
        inConstList.removeOne(link);
    }
    foreach (Link * link, outConstList) {
        myOutLinks.append(link);
        outConstList.removeOne(link);
    }
}

void Trans::showEditDialog() {
    /**
     * Metoda pro zavolani dialogu na editaci
     */
    transDialog->editDialog();
    showDialog();
}

void Trans::mouseDoubleClickEvent ( QGraphicsSceneMouseEvent * event ) {
    /**
     * Redefinice metody pro odchyceni dvojiteho poklepani na item
     * Spusti editacni dialog
     * \param event Odchyceny event.
     */
    if(event->button() == Qt::LeftButton)
        showEditDialog();
    // propagujeme dale
    QGraphicsObject::mouseDoubleClickEvent(event);
}

// Watch Item
WatchItem::WatchItem(QString newArg1, QString newArg2, QString newOp) {
    /**
     * @param newArg1 První argument podmínky
     * @param newArg2 Druhý argument podmínky
     * @param newOp Operátor
     * 
     * Konstruktor objektu vnitřní reprezentace watch
     */
    firstArg = newArg1;
    secondArg = newArg2;
    
    if      (newOp == "<")
        watchOp = OLOW;
    else if (newOp == "<=")
        watchOp = OLEQ;
    else if (newOp == ">=")
        watchOp = OGEQ;
    else if (newOp == ">")
        watchOp = OGRT;
    else if (newOp == "==")
        watchOp = OEQ;
    else if (newOp == "!=")
        watchOp = ONEQ;
}

QString WatchItem::getFirstArg() {
    /**
     * Navrací první argument podmínky
     */
    return firstArg;
}

QString WatchItem::getSecondArg() {
    /**
     * Navrací druhý argument podmínky
     */
    return secondArg;
}

myOps WatchItem::getWatchOp() {
    /**
     * Navrací operátor podmínky
     */
    return watchOp;
}
    
void WatchItem::setFirstArg(QString newArg) {
    /**
     * @param newArg Nahrazující řetězec
     * 
     * Nastaví první argument podmínky
     */
    firstArg = newArg;
}

void WatchItem::setSecondArg(QString newArg) {
    /**
     * @param newArg Nahrazující řetězec
     * 
     * Navrací druhý argument podmínky
     */
    secondArg = newArg;
}

void WatchItem::setWatchOp(myOps newOp) {
    /**
     * @param newOp Nahrazující operátor
     * 
     * Navrací operátor v podmínce
     */
    watchOp = newOp;
}


// BoundVarItem
BoundVarItem::BoundVarItem(QString newVar, int newVal) {
    /**
     * @param newVar Název proměnné
     * @param newVal Hodnota proměnné
     * 
     * Konstruktor objektu vnitřní reprezentace proměnné
     */
    var = newVar;
    val = newVal;
}

QString BoundVarItem::getVar() {
    /**
     * Metoda pro získání názvu proměnné
     */
    return var;
}

int BoundVarItem::getVal() {
    /**
     * Metoda pro získání hodnoty proměnné
     */
    return val;
}

void BoundVarItem::setVar(QString newVar) {
    /**
     * Nastaví jméno proměnné
     */
    var = newVar;
}

void BoundVarItem::setVal(int newVal) {
    /**
     * Nastaví hodnotu proměnné
     */
    val = newVal;
}
   

// TransEditDialog
TransEditDialog::TransEditDialog(Trans * newTrans, QWidget *parent) : QDialog(parent) {
    /**
     * @param newTrans Ukazatel na přechod, k němuž je dialog vázán
     * @param parent Ukazatel na QWidget, s kterým se dialog sváže
     * 
     * Konstruktor pro okno, které nastavuje hodnoty watch a výrazu na přechodu
     */
    label = new QLabel("Watch a vyraz:");
    watchLineEdit = new QLineEdit("watch");
    ExprLineEdit = new QLineEdit("vyraz");
    acceptButton = new QPushButton("OK");
    closeButton = new QPushButton("Cancel");
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(watchLineEdit);
    mainLayout->addWidget(ExprLineEdit);
    
    QVBoxLayout *buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(acceptButton);
    buttonLayout->addWidget(closeButton);
    
    mainLayout->addLayout(buttonLayout);
    
    setLayout(mainLayout);
    
    trans = newTrans;
}

void TransEditDialog::editDialog() {
    /**
     * Metoda, ktera pripravi dialog na editaci.
     */
    watchLineEdit->clear();
    ExprLineEdit->clear();
    watchLineEdit->insert(trans->getWatch());
    ExprLineEdit->insert(trans->getExpr());
    acceptButton->disconnect();
    connect(acceptButton, SIGNAL(clicked()), this, SLOT(editSlot()));
    
}

void TransEditDialog::acceptSlot() {
    /**
     * Slot upravující chování při kliknutí na OK v dialogu
     */
    if (watchLineEdit->text().isEmpty())
        trans->setWatch("    ");
    else
        trans->setWatch(watchLineEdit->text());
    if (ExprLineEdit->text().isEmpty())
        trans->setExpr("    ");
    else
        trans->setExpr(ExprLineEdit->text());
    emit addItem(trans);
    emit close();
}

void TransEditDialog::editSlot() {
    /**
     * Slot, ktery při kliknutí na OK v dialogu zmeni retezce straze a vyrazu
     */
    if (watchLineEdit->text().isEmpty())
        trans->setWatch("    ");
    else
        trans->setWatch(watchLineEdit->text());
    if (ExprLineEdit->text().isEmpty())
        trans->setExpr("    ");
    else
        trans->setExpr(ExprLineEdit->text());
    emit close();
}
