|-----------------------------------------------------------------------------|
Soubor: README.txt
Vytvořil: Stanislav Láznička
K projektu: [ICP]Petri Net Simulator

Autoři projektu: Petr Kubát <xkubat11@stud.fit.vutbr.cz>
		 Stanislav Láznička <xlazni08@stud.fit.vutbr.cz>
___________________________________________________________________

Popis:
	 Tento projekt implementuje simulátor Petriho sítí. Je navržen tak,
	že uživatel má možnost v grafickém rozhraní vytvořit návrh Petriho
	sítě, tento návrh odeslat na server, kde proběhne výpočet (dle uživa-
	telova přání jeden krok na daném přechodu nebo celá simulace).

Seznam některých možností projektu:
	- Vytvořit místa, přechody a spojení mezi nimi
	- Editace stávajících míst (tokeny), přechodů (watch, výraz)
	- Uložení grafického modelu do .xml struktury tohoto projektu
	- Načtení již uloženého modelu z .xml souboru
	- Zaslání na server pro výpočet kroku/celé simulace
	- a další...

První kroky:
	 Při navrhování rozhraní jsme se pokoušeli vycházet z jeho intuitivity.
	Po levé straně se nachází panel, který určuje, co chce uživatel dělat s
	objekty - může vytvářet místa/přechody/spojení, nebo případně s vytvo-
	řenými objekty hýbat.
	 Po vytvoření nějaké scény je možné odstartovat simulaci. To se udělá
	tak, že uživatel buď označí jeden objekt a klikne na tlačítko Step nebo
 	Sim (pro úplnou simulaci),to provede odeslání dat na server (uživatel
	bude optán na umístění serveru v dialogu), uživatel se musí autentizovat
	(!!!POZOR!!! - JE NASTAVEN PEVNÝ LOGIN NA LOGIN: admin HESLO: milous).
	Na serveru se provede kýžený výpočet a data se vrátí zpět. Nepovede-li
	 se autentizovat, server vrací zaslaná data v původní podobě.

Nedostatky:
	 Popsáno v dokumentaci : index->Ostatní stránky->Seznam plánovaných
	úprav
