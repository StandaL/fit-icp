#Author: Stanislav Láznička
#Date: 06-05-2012

SHELL = /bin/sh
DIRS = src/pn2012 src/server2012

all: pn2012 server2012

pn2012:
	cd src/pn2012; qmake fit-icp.pro; $(MAKE) $(MFLAGS); mv pn2012 ../../

server2012:
	cd src/server2012; qmake server.pro; $(MAKE) $(MFLAGS); mv server2012 ../../

doxygen: clean
	doxygen Doxyfile

clean:
	rm -f server2012 pn2012 xkubat11.tar.gz
	rm -rf doc/
	-for d in $(DIRS); do (cd $$d; $(MAKE) $(MFLAGS) clean; rm -f Makefile); done

pack: clean
	mkdir xkubat11
	cp -rf src/ examples/ Makefile Doxyfile README.txt xkubat11/
	tar -pczf xkubat11.tar.gz xkubat11
	rm -rf xkubat11

run: pn2012 
	./pn2012

runserver: server2012
	./server2012 8080
